from act.symbolic.symbol import Symbol
from act.symbolic.fields import Number
from act.symbolic.matrix import SymMatrix
from act.symbolic.binary_operators import Simplify

import numpy as np

def _sym(expression):
    expression = Number.to_Number_if_possible(expression)
    if isinstance(expression, Number):
        return expression
    if isinstance(expression, str):
        return Symbol(expression)
    if isinstance(expression, list):
        return SymMatrix(expression)
    if isinstance(expression,complex):
        return Complex(sym(expression.real), sym(expression.imag))
    if isinstance(expression, np.ndarray):
        return SymMatrix(expression.tolist())

    raise TypeError("Do not know how to handle %s" % repr(expression))


class Sym:
    def __call__(self, expression):
        return _sym(expression)
    def dot(self, A,B):
        return A.multiply(B)

sym = Sym()


simplify = Simplify()
