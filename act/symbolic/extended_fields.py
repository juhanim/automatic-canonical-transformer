from act.symbolic.fields import Field, Number, IntegerField, RationalField, ComplexField, Integer0, Integer1
from act.symbolic.binary_operators import Simplify
"""
Extended field is meant to make simplification process easier. An expression, which is 
completely numeric, but not containable within rational or complex rational field, for 
example because of fractional powers, be wrapped into a single Number with field
equating to AlgebraicField. This way, the main symbolic simplification can be simplified,
as possibly complicated numerical algebraic structures are wrapped into a single entity.

The first simplification rule of every expression:
   If expression is_numeric(), wrap it into a Number in AlgebraicField and simplify.
   (which can possibly yield a number in lower field).

"""

verbose = 0

def strip_field(number):
    if isinstance(number, Number) and number.field == AlgebraicField:
        return AlgebraicField.convert(strip_field(number.value))
    return AlgebraicField.convert(number)

class AlgebraicExpression:
    def __init__(self):
        pass

    def is_numeric(self):
        return True

    def flip_sign(self):
        return AlgebraicUnary(self)

    def simplify(self):
        return strip_field(self._simplify())

    def is_positive_real(self):
        z = complex(self)
        return z.imag == 0 and z.real > 0

    def is_negative_real(self):
        z = complex(self)
        return z.imag == 0 and z.real < 0

    def is_re_positive_or_zero(self):
        z = complex(self)
        return z.real >= 0

    def conjugate(self):
        return AlgebraicConjugate(self)

    def is_positive(self):
        z = complex(self)
        return z.real > 0

    def __gt__(self, o):
        if self.is_number() and not o.is_number():
            return False
        if not self.is_number() and o.is_number():
            return True
        return complex(self).real > complex(o).real

    def __eq__(self, o):
        raise NotImplementedError(self.__class__)

    def is_negative(self):
        z = complex(self)
        return z.real < 0

    def is_real(self):
        return abs(complex(self).imag)<1e-10

    def __complex__(self):
        raise NotImplementedError(self.__class__)

    def __float__(self):
        raise NotImplementedError(self.__class__)

    def __int__(self):
        assert self.is_integer()
        value = float(self)
        num = round(value)
        assert(abs(num-value)<1e-8)
        return num

    def __pow__(self, o):
        return AlgebraicPow(self, o).simplify()

    def __mul__(self, o):
        return AlgebraicMul(self, o).simplify()

    def __add__(self, o):
        return AlgebraicAdd(self, o).simplify()

    def __sub__(self, o):
        return AlgebraicSub(self, o).simplify()

    def __truediv__(self, o):
        return AlgebraicDiv(self, o).simplify()

class AlgebraicConjugate(AlgebraicExpression):
    def __init__(self, a):
        self.a = strip_field(a)

    def __eq__(self, o):
        if o.__class__ == self.__class__:
            return self.a == o.a
        if self.is_real() and o.is_real():
            return self.a == o
        return False

    def __repr__(self):
        return "conj(%s)" % repr(self.a)

    def conjugate(self):
        return self.a

    def __float__(self):
        return float(self.a)

    def is_number(self):
        return self.a.is_number()

    def is_unity(self):
        return self.a.is_unity()

    def is_integer(self):
        return self.a.is_integer()

    def is_real(self):
        return self.a.is_real()

    def is_zero(self):
        return self.a.is_zero()

    def is_positive(self):
        return self.a.is_positive()

    def is_negative(self):
        return self.a.is_negative()

    def __complex__(self):
        return complex(self.a).conjugate()

    def _simplify(self):
        if self.a.is_real():
            return self.a
        return self.a.simplify().conjugate()

class AlgebraicUnary(AlgebraicExpression):
    def __init__(self, a):
        self.a = strip_field(a)

    def __eq__(self, o):
        return o.__class__ == self.__class__ and self.a == o.a

    def __float__(self):
        return -float(self.a)

    def __repr__(self):
        return "Unary(%s)" % repr(self.a)

    def is_number(self):
        return False

    def is_unity(self):
        return False

    def is_integer(self):
        return self.a.is_integer()

    def is_real(self):
        return self.a.is_real()

    def is_zero(self):
        return self.a.is_zero()

    def is_positive(self):
        return self.a.is_negative()

    def is_negative(self):
        return self.a.is_positive()

    def __complex__(self):
        return -complex(self.a)

    def _simplify(self):
        if isinstance(self.a, AlgebraicUnary):
            return self.a.a.simplify()
        #if isinstance(self.a, AlgebraicMul):
        #    return AlgebraicMul(self.a.a.flip_sign(), self.a.b)
        #if isinstance(self.a, AlgebraicDiv):
        #    return AlgebraicDiv(self.a.a.flip_sign(), self.a.b)
        return self.a.simplify().flip_sign()

class AlgebraicBinaryOperator(AlgebraicExpression):
    def __init__(self, a, b, operator):
        a, b = Number.to_Number_if_possible(a), Number.to_Number_if_possible(b)

        self.a, self.b = strip_field(a), strip_field(b)
        self.operator = operator

    def __eq__(self, o):
        return self.__class__ == o.__class__ and self.a == o.a and self.b == o.b

    def is_number(self):
        return False

    def __repr__(self):
        return "%s(%s,%s)" % (self.__class__.__name__, repr(self.a), repr(self.b))

class AlgebraicInfinity(AlgebraicExpression):
    def is_zero(self):
        return False

    def is_unity(self):
        return False

    def _simplify(self):
        return self

class AlgebraicPow(AlgebraicBinaryOperator):
    def __init__(self, a, b):
        if a.is_zero() and b.is_negative():
            raise ZeroDivisionError
        AlgebraicBinaryOperator.__init__(self, a, b, "^")

    def is_zero(self):
        return self.a.is_zero() and b.positive()

    def conjugate(self):
        if self.is_real():
            return self
        else:
            return AlgebraicConjugate(self)

    def is_unity(self):
        return self.b.is_zero() or self.a.is_unity()

    def is_integer(self):
        return self.is_zero() or self.is_unity() or (a.is_integer() and b.is_integer() and b.is_positive_real())

    def __complex__(self):
        a = complex(self.a)
        b = complex(self.b)
        if a.imag == 0:
            a = a.real
        if b.imag == 0:
            b = b.real
        return complex(a**b)

    def __float__(self):
        return float(self.a)**float(self.b)

    def is_integer(self):
        if self.is_zero():
            return True
        if self.is_unity():
            return True
        return self.a.is_integer() and self.b.is_integer() and self.b.is_positive_real()

    def _simplify(self):
        a = self.a.simplify()
        b = self.b.simplify()
        if a.is_zero() and b.is_positive_real():
            return Integer0
        if a.is_zero() and b.is_zero():
            return Integer1
        if a.is_zero() and not b.is_positive_real():
            return AlgebraicInfinity()
        if b.is_zero():
            return Integer1
        if a.is_unity():
            return Integer1
        if isinstance(a, AlgebraicMul):
            if b.is_real() and a.a.is_re_positive_or_zero() and a.b.is_re_positive_or_zero():
                return AlgebraicMul((a.a**b).simplify(), (a.b**b).simplify()).simplify()
        if b.is_integer():
            if b.is_positive_real():
                if a.is_number() and a.field in [IntegerField, RationalField, ComplexField]:
                    return a**Number(int(b), IntegerField)
                #if int(b) < 4:
                #    print("EXPLICIT MULTIPLICATION", repr(a),repr(b))
                #    for i in range(int(b)):
                #        if i == 0:
                #            value = a
                #        else:
                #            value = value * a
                #    return value
            if b.is_negative_real() and a.is_positive_real():
                if int(b) > -4: # Do not expand too large powers
                    return (Integer1/(a**Number(-int(b), IntegerField))).simplify()


        if Simplify.RESOLVE_POW_CHAINS:   # (a.a^a.b)^b = a.a^(a.b*b)
            if isinstance(a, AlgebraicPow):
                if a.a.is_positive_real() and a.b.is_real() and b.is_real(): # or (a.b.is_integer() and b.is_integer()):
                    if verbose > 2:
                        Simplify.verbose_rule("RESOLVE_POW_CHAINS")
                    return AlgebraicPow(a.a, (a.b*b).simplify()).simplify()

        if a.is_positive_real() and b.is_number() and b.field == RationalField:
            # a.is_positive() required, since we are using principal root
            numerator,denominator = b.value
            if abs(numerator)<10 and abs(denominator)<10:
                return AlgebraicPow(
                            AlgebraicPow(a, Number(numerator, IntegerField)).simplify(), 
                            Number((1,denominator), RationalField))
        return AlgebraicPow(a, b)

class AlgebraicAdd(AlgebraicBinaryOperator):
    def __init__(self, a, b):
        AlgebraicBinaryOperator.__init__(self, a, b, "+")

    def is_integer(self):
        return a.is_integer() and b.is_integer()

    def conjugate(self):
        return AlgebraicAdd(self.a.conjugate(), self.b.conjugate())

    def is_zero(self):
        return self.a.is_zero() and self.b.is_zero()

    def is_unity(self):
        return (self.a.is_zero() and self.b.is_unity()) or (self.a.is_unity() and self.b.is_zero())

    def is_integer(self):
        return self.a.is_integer() and self.b.is_integer()

    def _simplify(self):
        a = self.a.simplify()
        b = self.b.simplify()
        if a.is_zero():
            return b
        if b.is_zero():
            return a
        if a.is_number() and b.is_number():
            return (a+b).simplify()
        if b.is_number(): # Numbers to front
            return AlgebraicAdd(b,a)
        return AlgebraicAdd(a,b)

    def __complex__(self):
        return complex(self.a)+complex(self.b)

    def __float__(self):
        return float(self.a)+float(self.b)

class AlgebraicSub(AlgebraicBinaryOperator):
    def __init__(self, a, b):
        AlgebraicBinaryOperator.__init__(self, a, b, "-")

    def conjugate(self):
        return AlgebraicSub(self.a.conjugate(), self.b.conjugate())

    def is_zero(self):
        return self.a == self.b

    def is_integer(self):
        return a.is_integer() and b.is_integer()

    def __complex__(self):
        return complex(self.a)-complex(self.b)

    def __float__(self):
        return float(self.a)-float(self.b)

    def is_unity(self):
        return self.a.is_unity() and self.b.is_zero()

    def is_integer(self):
        return self.a.is_integer() and self.b.is_integer()

    def _simplify(self):
        a = self.a.simplify()
        b = self.b.simplify()
        if a.is_number() and b.is_number():
            return a-b
        return AlgebraicSub(a,b)

class AlgebraicDiv(AlgebraicBinaryOperator):
    def __init__(self, a, b):
        AlgebraicBinaryOperator.__init__(self, a, b, "/")

    def is_integer(self):
        return False

    def conjugate(self):
        return AlgebraicDiv(self.a.conjugate(), self.b.conjugate())

    def is_zero(self):
        return self.a.is_zero()

    def is_positive_real(self):
        if not self.a.is_real() or not self.b.is_real():
            return False
        if self.is_zero():
            return False
        return not (self.a.is_positive_real() ^ self.b.is_positive_real()) # Not xor

    def is_unity(self):
        return self.a == self.b

    def __complex__(self):
        return complex(self.a)/complex(self.b)

    def __float__(self):
        return float(self.a)/float(self.b)

    def _simplify(self):
        a = self.a.simplify()
        b = self.b.simplify()
        if b.is_zero():
            raise ZeroDivisionError()
        if a.is_zero():
            return Integer0
        if a.is_number() and b.is_number():
            return (a/b).simplify()
        if a.is_integer() and b.is_integer():
            return Number((int(a),int(b)), RationalField)
        if a.is_unity() and isinstance(b, AlgebraicPow):
            return AlgebraicPow(b.a, b.b.flip_sign())

        if Simplify.RESOLVE_DIV_CHAINS:
            # a / (b.a / b.b) = a * b.b / b.a
            if isinstance(b, AlgebraicDiv):
                if verbose > 2:
                     Simplify.verbose_rule("RESOLVE_DIV_CHAINS")
                return ((a*b.b) / b.a).simplify()

        if Simplify.FLIP_POW_SIGN_ON_DIV:
            # a / (b.a ^ b.b) = a * b.a^(-b.b), if b.b is integer
            if isinstance(b, AlgebraicPow):
                if verbose > 2:
                     Simplify.verbose_rule("FLIP_POW_SIGN_ON_DIV")
                return a * AlgebraicPow(b.a.simplify(), b.b.flip_sign().simplify())

        return AlgebraicDiv(a, b)

class AlgebraicMul(AlgebraicBinaryOperator):
    def __init__(self, a, b):
        AlgebraicBinaryOperator.__init__(self, a, b, "*")

    def __complex__(self):
        return complex(self.a)*complex(self.b)

    def conjugate(self):
        return AlgebraicMul(self.a.conjugate(), self.b.conjugate())

    def __float__(self):
        return float(self.a)*float(self.b)

    def is_zero(self):
        return self.a.is_zero() or self.b.is_zero()

    def is_integer(self):
        return self.a.is_integer() and self.b.is_integer()

    def is_unity(self):
        return self.a.is_unity() and self.b.is_unity()

    def is_integer(self):
        return self.a.is_integer() and self.b.is_integer()

    def _simplify(self):
        a = self.a.simplify()
        b = self.b.simplify()
        if a.is_unity():
            return b
        if b.is_unity():
            return a
        if a.flip_sign().is_unity():
            return AlgebraicUnary(b)
        if b.flip_sign().is_unity():
            return AlgebraicUnary(a)
        if a.is_zero() or b.is_zero():
            return Integer0
        if a.is_number() and b.is_number():
            return (a*b).simplify()
        if b.is_number():  # NUMBERS TO FRONT
            return AlgebraicMul(b, a)
        if isinstance(b, AlgebraicMul):
            if a.is_number() and b.a.is_number():
                return AlgebraicMul((a*b.a).simplify(), b.b).simplify()

        if isinstance(a, AlgebraicUnary) and isinstance(b, AlgebraicUnary):
            return AlgebraicMul(a.a, b.a).simplify()
        if isinstance(b, AlgebraicUnary):
            return AlgebraicUnary(AlgebraicMul(a, b.a)).simplify()
        if isinstance(a, AlgebraicUnary):
            return AlgebraicUnary(AlgebraicMul(a.a, b)).simplify()

        if Simplify.PREFER_RHS_TIMES_CHAINS and isinstance(a, AlgebraicMul):
            # Mul( Mul(a,b), c) -> Mul(a, Mul(b, c))
            return AlgebraicMul(a.a, AlgebraicMul(a.b, b)).simplify()

        if Simplify.A_TIMES_A_IS_A_POW_2:
            if a == b:
                if verbose>2:
                    Simplify.verbose_rule("A_TIMES_A_IS_A_POW_2")
                return AlgebraicPow(a, 2).simplify()

        if Simplify.ORDER_TIMES_CHAINS and isinstance(b, AlgebraicMul):
            if a > b.a:
                if verbose > 2:
                    Simplify.verbose_rule("ORDER_TIMES_CHAINS")
                return (b.a * (a*b.b).simplify()).simplify()

        return AlgebraicMul(a, b)

""" Algebraic field means all possible numbers which can be generated by +-/*^ operations,
    and in addition, possible roots of polynomials. Simplification becomes faster, when there is just
    one instance representing possible complicated algebraic number expression. """
class AlgebraicFieldClass(Field):
    def type_check(self, number):
        pass

    def __init__(self):
        Field.__init__(self, 'AlgebraicField')

    def convert_number(self, number):
        nunber = self.convert(number)
        if isinstance(number, AlgebraicExpression): # Algebraic expressions are kept inside the number field
            return Number(number, AlgebraicField)
        return number

    def convert(self, number):
        from act.symbolic.binary_operators import Plus, Div, Times, Pow, Minus
        from act.symbolic.expression import UnaryMinus
        if isinstance(number, Plus):
            return AlgebraicAdd(self.convert(number.a), self.convert(number.b))
        if isinstance(number, Minus):
            return AlgebraicSub(self.convert(number.a), self.convert(number.b))
        if isinstance(number, Times):
            return AlgebraicMul(self.convert(number.a), self.convert(number.b))
        if isinstance(number, Div):
            return AlgebraicDiv(self.convert(number.a), self.convert(number.b))
        if isinstance(number, Pow):
            return AlgebraicPow(self.convert(number.a), self.convert(number.b))
        if isinstance(number, UnaryMinus):
            return AlgebraicUnary(self.convert(number.a))
        return number

    def number__init__(self, number):
        return self.convert(number)

    def _number__eq__(self, left, right):
        return left.value == right.value

    def _number__gt__(self, left, right):
        return left.value > right.value

    def number__float__(self, number):
        return float(number)

    def number__complex__(self, number):
        return complex(number.value)

    def number_promote_to_field(self, number):
        assert(isinstance(number, Number))
        return Number(number, AlgebraicField)

    def number_conjugate(self, number):
        return self.convert_number(number.value.conjugate())

    def number__str__(self, number):
        return str(number.value)

    def number__repr__(self, number):
        return "AlgebraicNumber(%s)" % repr(number.value)

    def number_is_negative(self, number):
        return number.is_negative

    def number_is_zero(self, number):
        return number.value.is_zero()

    def number_is_unity(self, number):
        return number.value.is_unity()

    def number_has_leading_unary_operator(self, number):
        return True # Only used for representations

    def number_flip_sign(self, number):
        return self.convert_number(number.value.flip_sign())

    def number_representation(self, number, caller=None, position=None):
        return repr(number.value)

    def number_simplify(self, number):
        assert isinstance(number, Number) and number.field == AlgebraicField
        return self.convert_number(number.value.simplify())

    def number_simplify_isolate_factor(self, number):
        return None, self.convert_number(number.value)

    def number__add__(self, left, right):
        assert isinstance(left, Number) and left.field == AlgebraicField
        assert isinstance(right, Number) and right.field == AlgebraicField
        return Number(AlgebraicAdd(left.value, right.value).simplify(), AlgebraicField)

    def number__sub__(self, left, right):
        assert isinstance(left, Number) and left.field == AlgebraicField
        assert isinstance(right, Number) and right.field == AlgebraicField
        return Number(AlgebraicSub(left.value, right.value).simplify(), AlgebraicField)

    def number__mul__(self, left, right):
        assert isinstance(left, Number) and left.field == AlgebraicField
        assert isinstance(right, Number) and right.field == AlgebraicField
        return Number(AlgebraicMul(left.value, right.value).simplify(), AlgebraicField)

    def number__pow__(self, left, right):
        assert isinstance(left, Number) and left.field == AlgebraicField
        assert isinstance(right, Number) and right.field == AlgebraicField
        return Number(AlgebraicPow(left.value, right.value).simplify(), AlgebraicField)

    def number__truediv__(self, left, right):
        assert isinstance(left, Number) and left.field == AlgebraicField
        assert isinstance(right, Number) and right.field == AlgebraicField
        return Number(AlgebraicDiv(left.value, right.value).simplify(), AlgebraicField)

AlgebraicField = AlgebraicFieldClass()
from act.symbolic.fields import ComplexField
ComplexField.subset(AlgebraicField)
