from copy import deepcopy
from act.display.symbols import pretty

class Operable:
    operator_priority = 0

    def commutes_with(self, o):
        raise NotImplementedError(self.__class__) # commutes_with

    def is_numeric(self):
        raise NotImplementedError(self.__class__) # is_numeric

    def simplify_numeric(self):
        raise NotImplementedError(self.__class__) # simplify_numeric

    def __add__(self, o): 
        raise NotImplementedError(self.__class__) # __add__

    def __gt__(self, other):
        raise NotImplementedError(self.__class__) # __gt__

    def __sub__(self, o):
        raise NotImplementedError(self.__class__) # __sub__

    def __pow__(self, o):
        raise NotImplementedError(self.__class__) # __pow__

    def __truediv__(self, o):
        raise NotImplementedError(self.__class__) # __truediv__

    def __neg__(self):
        raise NotImplementedError(self.__class__) # __neg__

    def __mul__(self, o): 
        raise NotImplementedError(self.__class__) # __mul__

    def __eq__(self, o):
        raise NotImplementedError(self.__class__) # __eq__

def sym(expression):
    if isinstance(expression, int):
        return Integer(expression)
    if isinstance(expression, str):
        return Symbol(expression)
    raise TypeError("Do not know how to handle %s" % repr(expression))

class Expression(Operable):
    precedence = 100
    def __init__(self):
        self._simplified_expression = None

    def __str__(self):
        return self.__class__.__name__

    def is_operator(self):
        return False

    def __add__(self, o): 
        from act.symbolic.binary_operators import Plus
        from act.symbolic.extended_fields import AlgebraicField
        return AlgebraicField.convert_number(Plus(self, o))

    def __radd__(self, o): 
        from act.symbolic.binary_operators import Plus
        from act.symbolic.extended_fields import AlgebraicField
        return AlgebraicField.convert_number(Plus(o, self))

    def __gt__(self, other):
        a_num, b_num = self.is_numeric(), other.is_numeric()
        #print(" %s > %s " % (repr(self), repr(other)))
        #print("numerics: %s, %s" % (a_num, b_num))
        if not a_num and b_num:
            #print("True")
            return True
        if a_num and not b_num:
            #print("False")
            return False
        if a_num and b_num:
            return float(a_num) > float(b_num)
        return repr(self) > repr(other)

    def __sub__(self, o): 
        from act.symbolic.binary_operators import Minus
        from act.symbolic.extended_fields import AlgebraicField
        return AlgebraicField.convert_number(Minus(self, o))

    def __pow__(self, o):
        from act.symbolic.binary_operators import Pow
        return Pow(self, o)

    def __truediv__(self, o):
        assert(not self.__class__=="SymMatrix")
        if isinstance(o, Operable):
            if o.operator_priority > self.operator_priority:
                return o.__rdiv__(self)
            else:
                pass
        else:
            pass
        from act.symbolic.extended_fields import AlgebraicField
        from act.symbolic.binary_operators import Div
        return AlgebraicField.convert_number(Div(self, o))

    def __neg__(self):
        return UnaryMinus(self)

    def __mul__(self, o): 
        if isinstance(o, Operable):
            if o.operator_priority > self.operator_priority:
                return o.__rmul__(self)
        from act.symbolic.binary_operators import Times
        from act.symbolic.extended_fields import AlgebraicField
        return AlgebraicField.convert_number(Times(self, o))

    def __rmul__(self, o):
        if isinstance(o, Operable):
            if o.operator_priority > self.operator_priority:
                return o.__mul__(self)
        from act.symbolic.binary_operators import Times
        from act.symbolic.extended_fields import AlgebraicField
        return AlgebraicField.convert_number(Times(o, self))

    def __eq__(self, o):
        raise NotImplementedError(self.__class__)

    def flip_sign(self):
        raise NotImplementedError(self.__class__)

    def simplify(self, verbose=0, lowlevel=False):
        if self.is_numeric():
            from act.symbolic.fields import Number
            from act.symbolic.extended_fields import AlgebraicField
            return Number(self, AlgebraicField).simplify(verbose, lowlevel)
        # Cache the simplification. All expressions are immutable.
        #if self._simplified_expression is None:
        self._simplified_expression = self._simplify(verbose, lowlevel)
        return self._simplified_expression

    def _simplify(self, verbose=0, lowlevel=False):
        raise NotImplementedError

    def simplify_isolate_factor(self, verbose=0):
        raise NotImplementedError(self.__class__)

    def __eq__(self, o):
        assert(isinstance(o, Expression))
        raise NotImplementedError(self.__class__)

    def conjugate(self):
        raise NotImplementedError(self.__class__)

    def is_number(self):
        raise NotImplementedError(self.__class__)
        raise NotImplementedError

    def is_unity(self):
        raise NotImplementedError(self.__class__)

    # Note: Only returning True can be taken as indication of anything.
    # A symbol might return False for is_negative, is_positive and is_zero, if its value is unknown

    def is_negative(self):
        raise NotImplementedError(self.__class__)

    def is_positive(self):
        raise NotImplementedError(self.__class__)

    def is_re_positive_or_zero(self):
        if self.is_real():
            return self.is_positive() or self.is_zero()
        raise NotImplementedError(self.__class__)

    def is_zero(self):
        raise NotImplementedError(self.__class__)

    def is_integer(self):
        raise NotImplementedError(self.__class__)

    def is_real(self):
        raise NotImplementedError(self.__class__)

    def is_positive_real(self):
        return self.is_real() and self.is_positive()

    def is_negative_real(self):
       return self.is_real() and self.is_negative()

    def representation(self, caller=None, position=None):
        raise NotImplementedError(self.__class__)

    def has_leading_unary_operator(self):
        raise NotImplementedError(self.__class__)

class UnaryMinus(Expression):
    precedence = 5
    def __init__(self, a):
        Expression.__init__(self)
        self.a = a

    def simplify_numeric(self):
        if self.a.is_numeric():
            return self.a.simplify_numeric().flip_sign()
        return self.a

    def is_unity(self):
        return False

    def is_operator(self):
        return True

    def is_zero(self):
        return self.a.is_zero()

    def is_number(self):
        return self.a.is_number()

    def __eq__(self, o):
        if not isinstance(o, UnaryMinus):
            return False
        return self.a == o.a

    def is_numeric(self):
        return self.a.is_numeric()

    def is_real(self):
        return self.a.is_real()
    
    def _simplify(self, verbose=0, lowlevel=False):
        a = self.a.simplify(verbose, lowlevel)
        return a.flip_sign()

    def has_leading_unary_operator(self):
        return True

    def __repr__(self):
        return "UnaryMinus(%s)" % repr(self.a)

    def simplify_isolate_factor(self, verbose=0):
        if self.a.is_number():
            return None, self.a.flip_sign()

        a, factor = self.a.simplify_isolate_factor(verbose)
        from act.symbolic.fields import IntegerMinus1
        return a, IntegerMinus1 * factor

    def flip_sign(self):
        return self.a

    def __str__(self):
        return "-%s" % str(self.a)

    def representation(self, caller=None, position=None):
        return "-%s" % self.a.representation(self, 1)
        if caller is None:
            return(str(elf))
        if self.value >= 0:
            return(str(self))
        if caller is not None:
            if position > 1:
                return "(%s)" % str(self)
        return str(self)

class Conjugate(Expression):
    def __init__(self, a):
        self.a = a

    def simplify(self, verbose=0, lowlevel=False):
        a = self.a.simplify()
        if a.is_real():
            return a
        return Conjugate(a)

    def flip_sign(self):
        return Conjugate(self.a.flip_sign())

    def is_real(self):
        return self.a.is_real()

    def has_leading_unary_operator(self):
        return True

    def __repr__(self):
        return "Conjugate(%s)" % repr(self.a)

    def simplify_isolate_factor(self, verbose=0):
        a, factor = self.a.simplify_isolate_factor(verbose)
        return a.conjugate(), factor.conjugate()

    def __str__(self):
        return "conj(%s)" % str(self.a)

    def representation(self, caller=None, position=None):
        return "conj(%s)" % self.a.representation(self)
