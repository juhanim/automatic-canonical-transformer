from act.symbolic.expression import Expression, UnaryMinus
from act.symbolic.fields import Number, IntegerField, Integer2, Integer1, Integer0

class Simplify:
    # 1. Define arbitrary comparison operation > between expressions
    #    a) When constructing an operation Op(a,b), and if a and b is commute, and operator is commutative and associative 
    #       Op(a,b) -> Op(b,a) iff a > b.
    ORDER_COMMUTATIVE_OPERANDS = True

    #a*b -> b*a if sym(b) > sym(c)
    ORDER_TIMES = True

    # (a+b)+c -> (a+c)+b if sym(b) > sym(c)
    ORDER_PLUS_CHAINS = True

    # (a*b)*c -> (a*c)*b if sym(b) > sym(c)
    ORDER_TIMES_CHAINS = True

    # 0+b = b
    ZERO_PLUS_B_IS_B = True

    # a+0 = a
    A_PLUS_ZERO_IS_A = True

    # a op b = c, if a and b are numbers
    NUMBER_OPERATOR_NUMBER = True

    # a+a = 2a
    A_PLUS_A_IS_2A = True

    # f1*a + f2*a = (f1+f2)*a
    PLUS_COMMON_FACTOR = True

    # f1*a - f2*a = (f1-f2)*a
    MINUS_COMMON_FACTOR = True

    # f1*(a+b) = f1*a + f1*b
    DISTRIBUTE_COMMON_FACTOR = True
	    

    # a*a = a^2
    A_TIMES_A_IS_A_POW_2 = True

    # a^n*a = a^(n+1)
    A_TIMES_A_POW_N = True

    # Plus(Plus(a,b), Plus(b,c)) = Plus(a, Plus(b, Plus(c, d))
    AVOID_PLUS_TREES = True

    # Plus(Plus(a,b), c) -> Plus(a, Plus(b, c))
    PREFER_RHS_PLUS_CHAINS = True

    # Times(Times(a,b), c) -> Times(a, Plus(b, c))
    PREFER_RHS_TIMES_CHAINS = True

    # Times(Times(a,b), Times(b,c)) = Times(a, Times(b, Times(c, d))
    AVOID_TIMES_TREES = True

    # f1*a + (f2*a + c) = (f1+f2)*a + c
    PLUS_CHAIN_COMMON_FACTOR = True

    # a * (a * b) =a^2 * b
    TIMES_CHAIN_TO_POW = True

    # a / (b / c) = a*c / b
    RESOLVE_DIV_CHAINS = True

    # (a^b)^c = a^(b*c), if a >0
    RESOLVE_POW_CHAINS = True

    # a^1 = a
    POWER_TO_UNITY = True

    # 1^a = 1
    UNITY_TO_POWER = True

    # (a/b)^c = (a^c) / (b^c)
    DIV_POWERS = True

    # a / (b^c) = a b^(-c)
    FLIP_POW_SIGN_ON_DIV = True

    # a^(-1) = 1/a
    POWER_MINUS_ONE_IS_DIV = True

    # 0^a = 0
    ZERO_TO_POWER = True

    def verbose_rule(rule):
        print("Simplification rule: %s" % rule)

    def __call__(self, expression, verbose=0):
        old_expression = expression
        for i in range(10):
            if verbose>0:
                print("Simplification step %d" % i, expression.representation())
            expression = expression.simplify(verbose)
            if old_expression == expression:
                break
            old_expression = expression
        return expression

class BinaryExpression(Expression):
    precedence = 0
    def __init__(self, a, b):
        Expression.__init__(self)
        self.a, self.b = Number.to_Number_if_possible(a), Number.to_Number_if_possible(b)

    def is_commutative(self):
        raise NotImplementedError

    def is_number(self):
        return False

    def is_unity(self):
        return False

    def is_operator(self):
        return True

    def is_numeric(self):
        return self.a.is_numeric() and self.b.is_numeric()

    def copy(self, a, b):
        raise NotImplementedError

    def has_leading_unary_operator(self):
        return self.a.has_leading_unary_operator()

    def __eq__(self, o):
        if not isinstance(o, self.__class__):
            return False
        return (self.a == o.a) and (self.b == o.b)

    def is_real(self):
        return a.is_real() and b.is_real()

    def simplify_numeric(self, a, b):
        return self.__class__(a.simplify_numeric(), b.simplify_numeric())

class Plus(BinaryExpression):
    precedence = 1
    def __init__(self, a, b):
        BinaryExpression.__init__(self, a, b)

    def copy(self, a, b):
        return Plus(a,b)

    def is_zero(self):
        return self.a.is_zero() and self.b.is_zero()

    def is_positive(self):
        return self.a.is_positive() and self.b.is_positive()

    def is_negative(self):
        return self.a.is_negative() and self.b.is_negative()

    def is_commutative(self):
        return True

    def __eq__(self, o):
        if not isinstance(o, Plus):
            return False
        return ((self.a == o.a) and (self.b == o.b)) or ((self.a == o.b) and (self.b == o.a))

    def flip_sign(self):
        return self.a.flip_sign() + self.b.flip_sign()

    def conjugate(self):
        return Plus(self.a.conjugate(), self.b.conjugate())

    def __repr__(self):
        return "Plus(%s, %s)" % (repr(self.a), repr(self.b))

    def _simplify(self, verbose=0, lowlevel=False):
        if verbose > 2:
            print("Plus.simplify: %s" % repr(self))
        a = self.a.simplify(verbose, lowlevel)
        b = self.b.simplify(verbose, lowlevel)

        if isinstance(b, Times) or isinstance(b, Div):
            if b.a.is_number() and b.a.is_negative():
                result = Minus(a, b.__class__(b.a.flip_sign(), b.b))
                result = result.simplify()
                return result
 
        if verbose > 3:
            print("Plus.simplify constituents returned")
            print("a = ", repr(a))
            print("b = ", repr(b))

        if Simplify.AVOID_PLUS_TREES and isinstance(a, Plus) and isinstance(b, Plus):
            if verbose > 2:
                Simplify.verbose_rule("AVOID_PLUS_TREES")
            return Plus(a.a, Plus(a.b, Plus(b.a, b.b)))

        if Simplify.PREFER_RHS_PLUS_CHAINS and isinstance(a, Plus):
            # Plus(Plus(a,b), c) -> Plus(a, Plus(b, c))
            result = Plus(a.a, Plus(a.b, b))
            if verbose > 2:
                Simplify.verbose_rule("PREFER_RHS_PLUS_CHAINS %s to %s." % (repr(self), repr(result)))
            return result

        if not lowlevel and Simplify.ORDER_COMMUTATIVE_OPERANDS:
            a_, factor_a = a.simplify_isolate_factor()
            b_, factor_b = b.simplify_isolate_factor()
            if b_ is None: # Pure numbers to left
                if verbose > 2:
                    Simplify.verbose_rule("ORDER_COMMUTATIVE_OPERANDS. Numbers to left.")
                a, b = b, a
            elif not isinstance(b_, Plus): # Do not create LHS operator chains
                if (a_ is not None and b_ is not None and b_ < a_) or b_ is None:
                    a, b = b, a
                    if verbose > 2:
                        Simplify.verbose_rule("ORDER_COMMUTATIVE_OPERANDS")

        if Simplify.ZERO_PLUS_B_IS_B and a.is_zero():
            if verbose > 2:
                Simplify.verbose_rule("ZERO_PLUS_B_IS_B")
            return b
        if Simplify.A_PLUS_ZERO_IS_A and b.is_zero():
            if verbose > 2:
                Simplify.verbose_rule("A_PLUS_ZERO_IS_A")
            return a

        if isinstance(b, Plus):
            a_, factor_a = a.simplify_isolate_factor()
            ba_, factor_ba = b.a.simplify_isolate_factor()
            bb_, factor_bb = b.b.simplify_isolate_factor()
            if Simplify.ORDER_PLUS_CHAINS:
                # (a + (ba + bb))
                if ba_ is None:
                    if verbose > 2:
                        Simplify.verbose_rule("ORDER_PLUS_CHAINS. Numbers to left.")
                    return b.a + (a+b.b).simplify()
                if a_ is not None and ba_ is not None and a_ > ba_:
                    if verbose > 2:
                        Simplify.verbose_rule("ORDER_PLUS_CHAINS")
                    return (b.a + (a+b.b).simplify())

            if Simplify.PLUS_CHAIN_COMMON_FACTOR:
                if a_ == ba_:
                    if verbose > 2:
                        Simplify.verbose_rule("PLUS_CHAIN_COMMON_FACTOR")
                    return ((factor_a+factor_ba)*a_ + b.b).simplify()

        if verbose > 3:
            print("a.simplify() = ", repr(a))
            print("b.simplify() = ", repr(b))
        
        if Simplify.NUMBER_OPERATOR_NUMBER and a.is_number() and b.is_number():
            if verbose > 2:
                Simplify.verbose_rule("NUMBER_OPERATOR_NUMBER")
            return a + b

        if Simplify.A_PLUS_A_IS_2A and a == b:
            if verbose > 2:
                Simplify.verbose_rule("A_PLUS_A_IS_2A")
            return Times(Integer2, a)

        if a.flip_sign() == b:
            if verbose > 2:
                print("Simplification rule: a-a = 0")
            return Integer0

        if isinstance(b, UnaryMinus):
            if verbose > 2:
                print("Simplification rule: a+(-b) = a-b")
            result = Minus(a, b.a).simplify(verbose)
            if verbose > 2:
                print("Simplification rule: a+(-b) = a-b, result = %s." % repr(result))
            return result

        if not lowlevel and Simplify.PLUS_COMMON_FACTOR:
            a_, factor_a = a.simplify_isolate_factor()
            b_, factor_b = b.simplify_isolate_factor()
            if a_ == b_:
                if verbose > 2:
                    Simplify.verbose_rule("PLUS_COMMON_FACTOR")
                factor = (factor_a+factor_b).simplify(lowlevel=True)
                #if verbose > 3:
                #    print(factor_a, factor_b, factor)
                if a_ is None:
                    return factor
                return Times(factor, a_)

        if isinstance(a, Div) and isinstance(b, Div):
            if a.b == b.b:
                return Div((a.a+b.a).simplify(verbose), a.b).simplify(verbose)
            return Div(a.a*b.b+b.a*a.b, a.b*b.b).simplify(verbose)

        return Plus(a, b)

    def simplify_isolate_factor(self, verbose=0):
        if verbose > 2:
            print("Plus.simplify")
            print("a = ", repr(self.a))
            print("b = ", repr(self.b))
        a = self.a.simplify(verbose)
        b = self.b.simplify(verbose)

        if a.is_numeric() and b.is_numeric():
            return None, (a+b).simplify(verbose, lowlevel=True)

        if verbose > 2:
            print("Plus.simplify returned")
            print("a = ", repr(a))
            print("b = ", repr(b))
        if a.is_zero():
            return b.simplify_isolate_factor()
        if b.is_zero():
            return a.simplify_isolate_factor()

        #if isinstance(self.b, Plus):
        #    # Plus(a, Plus(b, c))
        #    if self.b.a > self.a: # Arbitrary comparison for fixed order
        #        if self.a > self.b.b:
        #            return Plus(self.b.a, Plus(self.a, self.b.b)).simplify_isolate_factor()
        #        else:
        #            return Plus(self.b.a, Plus(self.b.b, self.a)).simplify_isolate_factor()

        if isinstance(b, UnaryMinus):
            if verbose > 2:
                print("Simplification rule (factor): a+(-b) = a-b")
            return Minus(a, b.a).simplify_isolate_factor()

        if isinstance(a, Div) and isinstance(b, Div):
            if a.b == b.b:
                return Div(a.a+b.a, a.b).simplify_isolate_factor()
            return Div(a.a*b.b+b.a*a.b, a.b*b.b).simplify_isolate_factor()

        if a.is_number() and b.is_number():
            return None, a+b
        a_, a_factor = a.simplify_isolate_factor()
        b_, b_factor = b.simplify_isolate_factor()
        if a_factor == b_factor:
            return (a_+b_).simplify(), a_factor
        return Plus(a, b), Integer1

    def representation(self, caller=None, position=None):
        rep = "(%s+%s)" % (self.a.representation(self, 1), self.b.representation(self, 2))
        #if caller is not None and isinstance(caller, BinaryExpression):
        #    if caller.precedence > self.precedence:
        #        rep = "(%s)" % rep
        return rep

    def __str__(self):
        return "%s+%s" % (str(self.a), str(self.b))

class Minus(BinaryExpression):
    precedence=2
    def __init__(self, a, b):
        #if b.is_number() and b.is_negative():
        #    a = UnaryMinus(a.flip_sign())
        BinaryExpression.__init__(self, a, b)

    def copy(self, a, b):
        return Minus(a,b)

    def is_commutative(self):
        return False

    def flip_sign(self):
        return Minus(self.b, self.a)

    def conjugate(self):
        return Minus(self.a.conjugate(), self.b.conjugate())

    def __repr__(self):
        return "Minus(%s, %s)" % (repr(self.a), repr(self.b))

    def is_zero(self):
        return self.a == self.b

    def _simplify(self, verbose=0, lowlevel=False):
        a = self.a.simplify(verbose)
        b = self.b.simplify(verbose)

        if a.is_zero():
            return b.flip_sign().simplify(verbose)

        if b.is_zero():
            return a.simplify(verbose)

        if b.flip_sign() == a:
            return (2*a).simplify()

        if isinstance(b, Times):
            if isinstance(b.a, Number) and b.a.is_negative():
                return a + b.a.flip_sign()*b.b

        if a == b:
            return Integer0        
        if isinstance(a, Div) and isinstance(b, Div):
            if a.b == b.b:
                return Div(a.a-b.a, a.b).simplify(verbose)
            return Div(a.a*b.b-b.a*a.b, a.b*b.b)

        if isinstance(a, Plus): # (aa+ab)-b
            ab_, factor_ab = a.b.simplify_isolate_factor()
            b_, factor_b = b.simplify_isolate_factor()
            if ab_ == b_:
                return a.a + (factor_ab-factor_b).simplify() * ab_

        if not lowlevel and Simplify.MINUS_COMMON_FACTOR:
            a_, factor_a = a.simplify_isolate_factor()
            b_, factor_b = b.simplify_isolate_factor()
            if a_ == b_:
                if verbose > 2:
                    Simplify.verbose_rule("MINUS_COMMON_FACTOR")
                factor = (factor_a-factor_b).simplify(lowlevel=True)
                if a_ is None:
                    return factor
                return Times(factor, a_)


        if a.is_number() and b.is_number():
            return a - b
        return Minus(a, b)

    def simplify_isolate_factor(self, verbose=0):
        a = self.a.simplify(verbose)
        b = self.b.simplify(verbose)

        if a.is_zero():
            return b.flip_sign().simplify_isolate_factor(verbose)

        if b.is_zero():
            return a.simplify_isolate_factor(verbose)

        if a == b:
            return None, Integer0
        if isinstance(a, Div) and isinstance(b, Div):
            if a.b == b.b:
                return Div(a.a-b.a, a.b).simplify_isolate_factor(verbose)
            return Div(a.a*b.b-b.a*a.b, a.b*b.b).simplify_isolate_factor(verbose)

        if a.is_number() and b.is_number():
            return None, a-b
        return Minus(a, b), Integer1

    def representation(self, caller=None, position=None):
        rep = "(%s-%s)" % (self.a.representation(self, 1), self.b.representation(self, 2))
        return rep

    def __str__(self):
        return "%s-%s" % (str(self.a), str(self.b))

class Times(BinaryExpression):
    precedence=3
    def __init__(self, a, b):
        a = Number.to_Number_if_possible(a)
        b = Number.to_Number_if_possible(b)
        #if a > b:
        #    a, b = b, a
        #if isinstance(b, Number):
        #    a, b = b, a
        BinaryExpression.__init__(self, a, b)

    def copy(self, a, b):
        return Times(a,b)

    def is_commutative(self):
        return True

    def flip_sign(self):
        return Times(self.a.flip_sign(), self.b)

    def conjugate(self):
        return Times(self.a.conjugate(), self.b.conjugate())

    def is_zero(self):
        return self.a.is_zero() or self.b.is_zero()

    def is_positive(self):
        if self.is_zero():
            return False
        return (self.a.is_positive() and self.b.is_positive()) or (self.a.is_negative() and self.b.is_negative())

    def is_negative(self):
        if self.is_zero():
            return False
        return (self.a.is_positive() and self.b.is_negative()) or (self.a.is_negative() and self.b.is_positive())

    def __eq__(self, o):
        if not isinstance(o, Times):
            return False
        return ((self.a == o.a) and (self.b == o.b)) or ((self.a == o.b) and (self.b == o.a))

    def __repr__(self):
        return "Times(%s, %s)" % (repr(self.a), repr(self.b))

    def simplify_isolate_factor(self, verbose=0):
        a = self.a.simplify()
        b = self.b.simplify()

        if isinstance(a, UnaryMinus) and isinstance(b, UnaryMinus):
            return Times(a.a, b.a).simplify_isolate_factor()
        if isinstance(b, UnaryMinus):
            return UnaryMinus(Times(a, b.a)).simplify_isolate_factor()
        if isinstance(a, UnaryMinus):
            return UnaryMinus(Times(a.a, b)).simplify_isolate_factor()

        if Simplify.ORDER_TIMES and a > b:
            if verbose > 2:
                Simplify.verbose_rule("ORDER_TIMES")
            return self.copy(b,a).simplify_isolate_factor()

        if Simplify.AVOID_TIMES_TREES and isinstance(a, Times) and isinstance(b, Times):
            if verbose > 2:
                Simplify.verbose_rule("AVOID_TIMES_TREES")
            return (a.a*(a.b*(b.a*b.b).simplify()).simplify()).simplify().simplify_isolate_factor()

        if isinstance(b, Times):
            if Simplify.TIMES_CHAIN_TO_POW:
                # a * (a * b) =a^2 * b
                if a == b.a:
                    if verbose > 2:
                        Simplify.verbose_rule("TIMES_CHAIN_TO_POW")
                    return (Pow(a, Integer2) * b.b).simplify().simplify_isolate_factor()

            a_, factor_a = a.simplify_isolate_factor()
            ba_, factor_ba = b.a.simplify_isolate_factor()
            bb_, factor_bb = b.b.simplify_isolate_factor()
            if Simplify.ORDER_TIMES_CHAINS:
                # (a * (ba * bb))
                if a_ is not None and ba_ is not None and a_ > ba_:
                    if verbose > 2:
                        Simplify.verbose_rule("ORDER_TIMES_CHAINS")
                    return (b.a * (a*b.b).simplify()).simplify().simplify_isolate_factor()

        if Simplify.A_TIMES_A_IS_A_POW_2:
            if a == b:
                if verbose>2:
                    Simplify.verbose_rule("A_TIMES_A_IS_A_POW_2")
                return Pow(a, 2), Integer1

        if Simplify.A_TIMES_A_POW_N:
            if isinstance(a, Pow) and a.a == b:
                if verbose>2:
                    Simplify.verbose_rule("A_TIMES_A_POW_N")
                return Pow(a.a, a.b+1).simplify_isolate_factor()
            if isinstance(b, Pow) and self.b.a == self.a:
                if verbose>2:
                    Simplify.verbose_rule("A_TIMES_A_POW_N")
                return Pow(b.a, b.b+1).simplify_isolate_factor()

        if isinstance(a, Times) and isinstance(b, Times):
            # Mul(a, Mul(b, c))
            if b.a > a:
                return Times(b.a, Times(a, b.b)).simplify_isolate_factor()
        # a*(1/b') = a/b'
        if isinstance(a, Div):
            if a.a.is_unity():
                return Div(b.simplify(verbose), a.b.simplify(verbose)).simplify_isolate_factor(verbose)
        # (1/b')*b = b/b'
        if isinstance(b, Div):
            if b.a.is_unity():
                return Div(a.simplify(), b.b.simplify()).simplify_isolate_factor()

        #Mul(a,Div(b,c)) -> Div(a*b,c)
        if isinstance(b, Div):
            return Div((a.simplify() * b.a.simplify()).simplify(), b.b.simplify()).simplify_isolate_factor()

        # Mul(Div(a,b),a') -> Div(a*a', b)
        if isinstance(a, Div):
            return Div((a.a.simplify() * b.simplify()).simplify(), a.b.simplify()).simplify_isolate_factor()
       
        a, factor_a = a.simplify_isolate_factor()
        assert(factor_a is not None)
        b, factor_b = b.simplify_isolate_factor()
        assert(factor_b is not None)
        #print("XXX", a,factor_a, b, factor_b)

        productants = [ x for x in [a, b] if x is not None ]
        if len(productants) == 2:
            result = productants[0] * productants[1]
        elif len(productants) == 1:
            result = productants[0]
        else:
            result = None

        #print("factor_a", factor_a, "factor_b", factor_b)
        #print(factor_a.__class__, factor_b.__class__)
        factor = (factor_a * factor_b).simplify(lowlevel=True)
        #print("Factor out", factor)
        if factor.flip_sign().is_unity():
            return UnaryMinus(result), Integer1

        return result, factor
          
    def _simplify(self, verbose=0, lowlevel=False):
        if verbose > 2:
            print("Times simplify(a = %s, b = %s)" % (repr(self.a), repr(self.b)))

        if Simplify.DISTRIBUTE_COMMON_FACTOR:
            if self.a.is_number() and isinstance(self.b, Plus):
                return Plus(Times(self.a, self.b.a), Times(self.a, self.b.b)).simplify()
            if self.a.is_number() and isinstance(self.b, Minus):
                return Minus(Times(self.a, self.b.a), Times(self.a, self.b.b)).simplify()

        if lowlevel:
            a = self.a.simplify(verbose, lowlevel)
            b = self.b.simplify(verbose, lowlevel)
            if a.is_zero() or b.is_zero():
               return Integer0
            if a.is_unity():
                return b
            if b.is_unity():
                return a
            if a.is_number() and b.is_number():
                return (a*b).simplify(verbose, lowlevel)
            return a*b
        result, factor = self.simplify_isolate_factor(verbose)
        if verbose > 2:
            print("Times.simplify: Factor isolation: %s * %s" % (repr(result), repr(factor)))

        if factor is not None and factor.is_zero():
            if verbose > 2:
                print("Times.simplify: Factor isolation: factor=0, returning 0.")
            return Integer0
        if factor is None or factor.is_unity():
            if result is None:
                return Integer1
            else:
                return result
        if result is None or result.is_unity():
            return factor
        assert(factor is not None)
        assert(result is not None)
        if isinstance(result, Div):
            return (factor*result.a)/result.b
        result = Times(factor, result)
        if verbose > 2:
            print("Times.simplify: exiting with %s." % repr(result))
        return result

    def flip_sign(self):
        return Times(self.a.flip_sign(), self.b)

    def __str__(self):
        return "%s*%s" % (str(self.a), str(self.b))

    def __repr__(self):
        return "Times(%s,%s)" % (repr(self.a), repr(self.b))

    def representation(self, caller=None, position=None):
        arep = self.a.representation(self, 1)
        brep = self.b.representation(self, 2)
        wrapa, wrapb = False, False
        if self.a.precedence < self.precedence:
            wrapa = True
        if self.b.precedence < self.precedence:
            wrapb = True
        if caller is not None and self.a.has_leading_unary_operator() and position > 1:
            wrapa = True
        return "%s*%s" % ("(%s)" % arep if wrapa else arep, "(%s)" % brep if wrapb else brep)

# TODO: Combine classes, this is largely copy paste from Times
class Div(BinaryExpression):
    precedence=4
    def __init__(self, a, b):
        #if a.is_number() and a.is_negative():
        #    a = UnaryMinus(a.flip_sign())
        BinaryExpression.__init__(self, a, b)

    def copy(self, a, b):
        return Div(a,b)

    def is_commutative(self):
        return False

    def conjugate(self):
        return Div(self.a.conjugate(), self.b.conjugate())

    def __repr__(self):
        return "Div(%s, %s)" % (repr(self.a), repr(self.b))

    def __float__(self):
        return float(self.a) / float(self.b)

    def is_zero(self):
        return self.a.is_zero()

    def flip_sign(self):
        return Div(self.a.flip_sign(), self.b)

    def simplify_isolate_factor(self, verbose=0):
        if verbose > 2:
            print("Div.simplify_isolate_factor")
        if self.a == self.b:
            return None, Integer1
        if self.a.is_number() and self.b.is_number():
            return None, self.a.divide_number(self.b)
        a, factor_a = self.a.simplify_isolate_factor(verbose)
        b, factor_b = self.b.simplify_isolate_factor(verbose)
        factor = (factor_a / factor_b).simplify(verbose, lowlevel=True)
        assert(factor.is_number)
        if a is None and b is None:
            return None, factor
        if a is not None and b is not None:
            return Div((factor*a).simplify(verbose), b), Integer1 # Important that factors are not taken outside of div
        if a is not None:
            return a, factor
        if b is not None:
            return Div(factor, b), Integer1 # See above
    
    def _simplify(self, verbose=0, lowlevel=False):
        if verbose > 2:
            print("Div.simplify")

        a = self.a.simplify(verbose, lowlevel)
        b = self.b.simplify(verbose, lowlevel)

        if isinstance(a, UnaryMinus) and isinstance(b, UnaryMinus):
            return Div(a.a, b.a)
        if isinstance(b, UnaryMinus):
            return UnaryMinus(Div(a, b.a))
        if isinstance(a, UnaryMinus):
            return UnaryMinus(Div(a.a, b))

        if a.is_zero():
            return Integer0
        if a == b:
            return Integer1

        if Simplify.RESOLVE_DIV_CHAINS:
            # a / (b.a / b.b) = a * b.b / b.a
            if isinstance(b, Div):
                if verbose > 2:
                     Simplify.verbose_rule("RESOLVE_DIV_CHAINS")
                return ((a*b.b) / b.a).simplify()

        if Simplify.FLIP_POW_SIGN_ON_DIV:
            # a / (b.a ^ b.b) = a * b.a^(-b.b)
            if isinstance(b, Pow):
                if verbose > 2:
                     Simplify.verbose_rule("FLIP_POW_SIGN_ON_DIV")
                return a * Pow(b.a.simplify(), b.b.flip_sign().simplify())

        if lowlevel:
            if self.b.is_unity():
                return a
            if self.a.is_zero():
                return Integer0
            return a/b
        result, factor = self.simplify_isolate_factor(verbose)
        if factor.is_zero():
            return Integer0
        if factor.is_unity():
            return result
        if result is None or result.is_unity():
            return factor
        return Times(factor, result)

    def __str__(self):
        return "%s/%s" % (str(self.a), str(self.b))

    def representation(self, caller=None, position=None):
        arep = self.a.representation(self, 1)
        brep = self.b.representation(self, 2)
        wrapa, wrapb = False, False
        if self.a.precedence < self.precedence:
            wrapa = True
        if self.b.precedence < self.precedence:
            wrapb = True
        if caller is not None and self.a.has_leading_unary_operator() and position > 1:
            wrapa = True
        return "%s/%s" % ("(%s)" % arep if wrapa else arep, "(%s)" % brep if wrapb else brep)

# TODO: Combine classes, this is largely copy paste from Times
class Pow(BinaryExpression):
    precedence=5
    def __init__(self, a, b):
        BinaryExpression.__init__(self, a, b)

    def __repr__(self):
        return "Pow(%s, %s)" % (repr(self.a), repr(self.b))

    def copy(self, a, b):
        return Pow(a,b)

    def is_positive(self):
        return self.a.is_positive() and self.b.is_positve()

    def is_negative(self):
        return False
    
    def is_zero(self):
        if self.a.is_zero() and self.b.is_real_positive():
            return True
        return False

    def is_commutative(self):
        return False

    def conjugate(self):
        if self.a.is_real() and self.b.is_real() and self.b.is_integer():
            return self
        assert(self.b.is_integer())
        return Pow(self.a.conjugate(), self.b)

    def simplify_isolate_factor(self, verbose=0):
        return Pow(self.a.simplify(verbose), self.b.simplify(verbose)), Integer1
    
    def _simplify(self, verbose=0, lowlevel=False):
        a = self.a.simplify()
        b = self.b.simplify()
        if Simplify.POWER_TO_UNITY and b.is_unity():
            return a

        if Simplify.UNITY_TO_POWER and a.is_unity():
            return Integer1

        if Simplify.ZERO_TO_POWER and a.is_zero():
            return Integer0

        if Simplify.RESOLVE_POW_CHAINS:   # (a.a^a.b)^b = a.a^(a.b*b)
            if isinstance(a, Pow):
                if a.is_positive() or (a.b.is_integer() and b.is_integer()):
                    if verbose > 2:
                        Simplify.verbose_rule("RESOLVE_POW_CHAINS")
                    return Pow(a.a, a.b*b).simplify()

        if Simplify.DIV_POWERS:
            if isinstance(a, Div) and a.is_positive():
                if verbose > 2:
                    Simplify.verbose_rule("DIV_POWERS")
                return Pow(a.a.simplify(), b) / Pow(a.b.simplify(), b)

        if Simplify.POWER_MINUS_ONE_IS_DIV:
            if b.flip_sign().is_unity():
                return Integer1 / a

        return Pow(self.a.simplify(verbose), self.b.simplify(verbose))

    def flip_sign(self):
        return UnaryMinus(self)

    def __str__(self):
        return "%s^(%s)" % (str(self.a), str(self.b))

    def representation(self, caller=None, position=None):
        arep = self.a.representation(self, 1)
        brep = self.b.representation(self, 2)
        wrapa, wrapb = False, False
        if self.a.precedence < self.precedence:
            wrapa = True
        if self.b.precedence < self.precedence:
            wrapb = True
        if caller is not None and self.a.has_leading_unary_operator() and position > 1:
            wrapa = True
        return "%s^(%s)" % (arep, brep)
        #return "%s^%s" % ("(%s)" % arep if wrapa else arep, "(%s)" % brep if wrapb else brep)

