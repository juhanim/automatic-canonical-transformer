from act.symbolic.expression import Expression, UnaryMinus
from act.display.symbols import pretty
from act.symbolic.fields import Integer1

class Symbol(Expression):
    def __init__(self, symbol, is_complex=False):
        self.symbol = symbol
        self.is_complex = is_complex

    def __str__(self):
        return pretty(self.symbol)

    def is_real(self):
        return not self.is_complex

    def is_number(self):
        return False

    def is_positive(self):
        return True

    def is_negative(self):
        return False

    def is_real(self):
        return not self.is_complex

    def is_zero(self):
        return False

    def is_numeric(self):
        return False

    def __gt__(self, o):
        if o.is_numeric():
            return True
        if not isinstance(o, Symbol):
            return not o > self
        return self.symbol > o.symbol

    def __eq__(self, o):
        if not isinstance(o, Symbol):
            return False
        return (self.symbol == o.symbol) and (self.is_complex == o.is_complex)

    def simplify(self, verbose=0, lowlevel=False):
        return self

    def conjugate(self):
        if self.is_complex:
            raise NotImplementedError
        return self

    def flip_sign(self):
        return UnaryMinus(self)

    def is_unity(self):
        return False

    def __repr__(self):
        return "Symbol(%s)" % self.symbol

    def simplify_isolate_factor(self, verbose=0):
        return self, Integer1

    def representation(self, caller=None, position=None):
        return pretty(self.symbol)

    def has_leading_unary_operator(self):
        return False
