from act.symbolic.expression import Expression

from fractions import gcd, Fraction as builtin_Fraction

class Field:
    # Field object performs addition, substraction, multiplication and dividision operations on two given numbers.
    # The numbers need not to be in the same field, but they need to convertible to the same field.
    # For example, a sequence of fields could be Integer subset Rational subset ComplexRational

    operators = '+-*/^'

    # Subclasses need to implement following methdods
    # * number_is_negative
    # * number_is_positive
    # * number_is_zero
    # * number_is_unity
    # * number_has_leading_unary_operator
    # * number_flip_sign
    # * number_representation
    # * number_simplify
    # * number_simplify_isolate_factor
    # * number__str__
    # * number__repr__
    # * number__add__
    # * number__sub__
    # * number__mul__
    # * number__div__

    # unique_priority is a hint whether another operand is beyond or below in subset chain
    def __init__(self, name):
        self.name = name
        self.sub_field = None 
        self.super_field = None
        self.unique_priority = 0

    def __str__(self):
        return self.name

    def subset(self, field):
        assert self.super_field is None
        assert field.sub_field is None
        self.super_field = field
        field.sub_field = self
        field.promote_unique_priority_to_at_least(self.unique_priority+1)
        return field

    def promote_unique_priority_to_at_least(self, value):
        if self.super_field is not None:
            self.super_field.promote_unique_priority_to_at_lest(value+1)
        self.unique_priority = max(value, self.unique_priority)

    # Assumes the other is also a member of the field.
    def _operate(self, operator, left, right):
        if operator == '+':
            return self.number__add__(left, right)
        if operator == '-':
            return self.number__sub__(left, right)
        if operator == '*':
            return self.number__mul__(left, right)
        if operator == '/':
            return self.number__truediv__(left, right)
        if operator == '^':
            return self.number__pow__(left, right)
        raise NotImplementedError('Operator:%s' % operator)

    def _number__eq__(self, other):
        raise NotImplemtedError(self.__class__)

    def _number__gt__(self, other):
        raise NotImplemtedError(self.__class__)

    def _number__pow__(self, other):
        raise NotImplemtedError(self.__class__)

    def number__init__(self, other):
        raise NotImplementedError(self.__class__) # number__init__

    # operate
    def operate(self, operator, left, right):
        assert operator in self.operators
        p_left, p_right = left.field.unique_priority, right.field.unique_priority
        if p_left == p_right:
            return self._operate(operator, left, right)
        if p_left > p_right:
            return left.field._operate(operator, left, left.field.number_promote_to_field(right))
        #if p_right > p_leftt:
        return right.field._operate(operator, right.field.number_promote_to_field(left), right)

    def number__eq__(self, left, right):
        p_left, p_right = left.field.unique_priority, right.field.unique_priority
        if p_left == p_right:
            return left.field._number__eq__(left, right)
        if p_left > p_right:
            return left.field._number__eq__(left, left.field.number_promote_to_field(right))
        return right.field._number__eq__(right.field.number_promote_to_field(left), right)

    def number__gt__(self, left, right):
        p_left, p_right = left.field.unique_priority, right.field.unique_priority
        if p_left == p_right:
            return left.field._number__gt__(left, right)
        if p_left > p_right:
            return left.field._number__gt__(left, left.field.number_promote_to_field(right))
        return right.field._number__gt__(right.field.number_promote_to_field(left), right)

    def neutral_element(self):
        raise NotImplementedError(self.__class__)

    def identity_element(self):
        raise NotImplementedError(self.__class__)

    def number_conjugate(self, number):
        raise NotImplementedError(self.__class__)

    def number__str__(self, number):
        raise NotImplementedError(self.__class__)

    def number__float__(self, number):
        raise NotImplementedError(self.__class__)

    def number__complex__(self, number):
        raise NotImplementedError(self.__class__)

    def number__repr__(self, number):
        raise NotImplementedError(self.__class__)

    def number_is_negative(self, number):
        raise NotImplementedError(self.__class__)

    def number_is_real(self, number):
        raise NotImplementedError(self.__class__)

    def number_is_zero(self, number):
        raise NotImplementedError(self.__class__)

    def number_is_positive(self, number):
        raise NotImplementedError(self.__class__)

    def number_is_unity(self, number):
        raise NotImplementedError(self.__class__)

    def number_has_leading_unary_operator(self, number):
        raise NotImplementedError(self.__class__)

    def number_flip_sign(self, number):
        raise NotImplementedError(self.__class__)

    def number_representation(self, number, caller=None, position=None):
        raise NotImplementedError(self.__class__)

    def number__float__(self, number):
        return float(number.value)

    def number_promote_to_field(self, number):
        raise NotImplementedError

    def number_simplify(self, number):
        raise NotImplementedError

    def number_simplify_isolate_factor(self, number):
        raise NotImplementedError

    def number__add__(self, left, right):
        raise NotImplementedError

    def number__sub__(self, left, right):
        raise NotImplementedError

    def number__mul__(self, left, right):
        raise NotImplementedError

    def number__truediv__(self, left, right):
        raise NotImplementedError

    def number_is_integer(self, number):
        raise NotImplementedError

class IntegerFieldClass(Field):
    def type_check(self, number):
        assert(isinstance(number.value, int))

    def __init__(self):
        Field.__init__(self, 'IntegerField')

    def number__init__(self, value):
        assert(isinstance(value, int))
        return value

    def number_is_integer(self, number):
        return True

    def number_is_real(self, number):
        return True

    def number__float__(self, number):
        return float(number.value)

    def number__pow__(self, left, right):
        if left.is_zero() and right.is_negative():
            raise ZeroDivisionError
        if left.is_zero():
            return Integer0
        if right.is_unity():
            return left
        if right.is_integer() and right.is_positive():
            return Number(left.value**int(right), IntegerField).simplify()
        else:
            from act.symbolic.extended_fields import AlgebraicPow, AlgebraicField
            return Number(AlgebraicPow(left, right).simplify(), AlgebraicField)

    def _number__eq__(self, left, right):
        return left.value == right.value

    def _number__gt__(self, left, right):
        return left.value > right.value

    def number__int__(self, number):
        return number.value

    def number__complex__(self, number):
        return complex(self.number__float__(number))

    def number__str__(self, number):
        return "%d" % (number.value)

    def number__repr__(self, number):
        return "Int(%d)" % number.value

    def number_is_negative(self, number):
        return number.value < 0

    def number_is_positive(self, number):
        return number.value > 0

    def number_is_zero(self, number):
        return number.value == 0

    def number_is_unity(self, number):
        return number.value == 1

    def number_has_leading_unary_operator(self, number):
        return number.is_negative()

    def number_flip_sign(self, number):
        return Number(-number.value, IntegerField)

    def number_representation(self, number, caller=None, position=None):
        return self.number__str__(number)

    def number_promote_to_field(self, number):
        assert(isinstance(number.field, IntegerFieldClass))
        return number

    def number_simplify(self, number):
        return number

    def number_simplify_isolate_factor(self, number):
        return None, number

    def number_conjugate(self, number):
        return number

    def number__add__(self, left, right):
        return Number(left.value + right.value, IntegerField)

    def number__sub__(self, left, right):
        return Number(left.value - right.value, IntegerField)

    def number__mul__(self, left, right):
        return Number(left.value * right.value, IntegerField)

    def number__truediv__(self, left, right):
        return Number((left.value, right.value), RationalField)

class RationalFieldClass(Field):
    def __init__(self):
        Field.__init__(self, 'RationalField')

    def number__init__(self, value):
        a,b = value
        if b == 0:
            raise ZeroDivisionError()
        return self._simplify(value)

    def type_check(self, number):
        try:
            assert(isinstance(number.value[0], int))
            assert(isinstance(number.value[1], int))
        except Exception as e:
            print(number.value)
            raise e
  
    def number_conjugate(self, number):
        raise number

    def number__pow__(self, left, right):
        if left.is_zero() and right.is_negative():
            raise ZeroDivisionError
        assert left.field == RationalField
        self.type_check(left)
        if left.is_zero():
            return Integer0
        if right.is_unity():
            return left
        a, b = left.value
        if right.is_integer():
            if right.is_positive():
                return Number((a**int(right), b**int(right)), RationalField)
            else:
                return Number((b**(-int(right)), a**(-int(right))), RationalField)
        else:
            from act.symbolic.extended_fields import AlgebraicPow, AlgebraicField
            return Number(AlgebraicPow(left, right).simplify(), AlgebraicField)

    def _number__eq__(self, left, right):
        return left.value == right.value

    def _number__gt__(self, left, right):
        return left.value > right.value

    def number__float__(self, number):
        self.type_check(number)
        a, b = number.value
        return float(a/b)

    def number__int__(self, number):
        assert(number.is_integer())
        a, b = number.value
        return int(a/b)

    def number_is_integer(self, number):
        a, b = number.value
        return a % b == 0

    def number__complex__(self, number):
        self.type_check(number)
        return complex(self.number__float__(number))

    def number_promote_to_field(self, number):
        if isinstance(number.field, IntegerFieldClass):
            return Number((number.value, 1), RationalField)
        if isinstance(number.field, RationalFieldClass):
            return number
        raise TypeError("Cannot promote %s to RationalField." % str(number))

    def number__str__(self, number):
        self.type_check(number)
        return "%d/%d" % number.value

    def number__repr__(self, number):
        self.type_check(number)
        return "Rational(%d, %d)" % number.value

    def number_is_negative(self, number):       
        return number.value[0]*number.value[1] < 0

    def number_is_positive(self, number):       
        return number.value[0]*number.value[1] > 0

    def number_is_real(self, number):       
        return True

    def number_is_zero(self, number):
        self.type_check(number)
        return number.value[0] == 0

    def number_is_unity(self, number):
        self.type_check(number)
        return number.value[0] == number.value[1]

    def number_has_leading_unary_operator(self, number):
        self.type_check(number)
        return number.is_negative()

    def number_flip_sign(self, number):
        self.type_check(number)
        return Number((-number.value[0], number.value[1]), RationalField)

    def number_representation(self, number, caller=None, position=None):
        self.type_check(number)
        return self.number__str__(number)

    def _simplify(self, value):
        a, b = value
        factor = gcd(a, b)
        return a // factor, b // factor

    def number_simplify(self, number):
        self.type_check(number)
        # TODO: Greatest common factor
        a, b = self._simplify(number.value)
        if b == 1:
            return Number(a, IntegerField)
        return Number((a,b), RationalField)

    def number_simplify_isolate_factor(self, number):
        self.type_check(number)
        return None, self.number_simplify(number)

    def number__add__(self, left, right):
        self.type_check(left)
        self.type_check(right)
        (a, b), (c, d) = left.value, right.value
        return Number((a*d+b*c, b*d), RationalField)

    def number__sub__(self, left, right):
        self.type_check(left)
        self.type_check(right)
        (a, b), (c, d) = left.value, right.value
        return Number((a*d-b*c, b*d), RationalField)

    def number__mul__(self, left, right):
        self.type_check(left)
        self.type_check(right)
        (a, b), (c, d) = left.value, right.value
        return Number((a*c, b*d), RationalField)

    def number__truediv__(self, left, right):
        self.type_check(left)
        self.type_check(right)
        (a, b), (c, d) = left.value, right.value
        return Number((a*d, b*c), RationalField)

class ComplexFieldClass(Field):
    def type_check(self, number):
        assert(isinstance(number.value[0], Number))
        assert(isinstance(number.value[1], Number))

    def __init__(self):
        Field.__init__(self, 'ComplexField')

    def _number__eq__(self, left, right):
        return left.value == right.value

    def number__init__(self, value):
       return value

    def number__int__(self, number):
        r, i = number.value
        assert(i.is_zero())
        assert(r.is_integer())
        return int(r)

    def number_is_integer(self, number):
        r, i = number.value
        return r.is_integer() and i.is_zero()

    def _number__gt__(self, left, right):
        return left.value > right.value

    def number__float__(self, number):
        self.type_check(number)
        r, i = number.value
        if not i.is_zero():
            raise TypeError("Cannot convert complex into float.")
        return float(r)

    def number__complex__(self, number):
        self.type_check(number)
        r, i = number.value
        return float(r)+1j*float(i)

    def number_promote_to_field(self, number):
        if isinstance(number.field, IntegerFieldClass):
            return Number((number, Number(0, IntegerField)), ComplexField)
        if isinstance(number.field, RationalFieldClass):
            return Number((number, Number(0, IntegerField)), ComplexField)
        raise TypeError("Cannot promote %s to RationalField." % str(number))

    def number_conjugate(self, number):
        r, i = number.value
        return Number((r, i.flip_sign()), ComplexField)

    def number__str__(self, number):
        self.type_check(number)
        r, i = number.value
        if i.is_zero():
            return str(r)
        if r.is_zero():
            return "%si" % str(i)
        return "(%s+%si)" % (str(r), str(i))

    def number__repr__(self, number):
        self.type_check(number)
        r, i = number.value
        return "Complex(%s, %s)" % (str(r),str(i))

    def number_is_negative(self, number):
        self.type_check(number)
        r, i = number.value
        return r.is_negative()

    def number_is_real(self, number):
        self.type_check(number)
        r, i = number.value
        return i.is_zero()

    def number_is_positive(self, number):
        self.type_check(number)
        r, i = number.value
        return r.is_positive()

    def number_is_zero(self, number):
        self.type_check(number)
        r, i = number.value
        return r.is_zero() & i.is_zero()

    def number_is_unity(self, number):
        self.type_check(number)
        r, i = number.value
        return r.is_unity() & i.is_zero()

    def number_has_leading_unary_operator(self, number):
        self.type_check(number)
        return True # This is only for printing. Better assume this for now.

    def number_flip_sign(self, number):
        self.type_check(number)
        r, i = number.value
        return Number((r.flip_sign(), i.flip_sign()), ComplexField)

    def number_representation(self, number, caller=None, position=None):
        self.type_check(number)
        return self.number__str__(number)

    def number_simplify(self, number):
        self.type_check(number)
        r, i = number.value
        r, i = r.simplify(), i.simplify()      
        if i.is_zero():
            return r
        return Number((r,i), ComplexField)

    def number_simplify_isolate_factor(self, number):
        self.type_check(number)
        return None, self.number_simplify(number)

    def number__add__(self, left, right):
        self.type_check(left)
        self.type_check(right)
        (a, b), (c, d) = left.value, right.value
        return Number((a+c, b+d), ComplexField)

    def number__sub__(self, left, right):
        self.type_check(left)
        self.type_check(right)
        (a, b), (c, d) = left.value, right.value
        return Number((a-c, b-d), ComplexField)

    def number__mul__(self, left, right):
        self.type_check(left)
        self.type_check(right)
        (a, b), (c, d) = left.value, right.value
        return Number((a*c-b*d, a*d+b*c), ComplexField)

    def number__pow__(self, left, right):
        self.type_check(left)
        self.type_check(right)
        if right.is_integer():
            for i in range(int(right)):
                if i == 0:
                    value = left
                else:
                    value = value * left
            return value
        return Number(AlgebraicPow(left, right), AlgebraicField)

    def number__truediv__(self, left, right):
        self.type_check(left)
        self.type_check(right)
        (a, b), (c, d) = left.value, right.value
        return Number(( (a*c+b*d)/(c*c+d*d), (b*c-a*d)/(c*c+d*d) ), ComplexField)

class Number(Expression):
    def __init__(self, value, field):
        self.value = field.number__init__(value)
        self.field = field
        field.type_check(self)

    def is_numeric(self):
        return True

    def is_re_positive_or_zero(self):
        if self.is_real():
            return self.is_positive() or self.is_zero()
        return complex(self).real >= 0

    def commutes_with(self, o):
        # Number commutes with everything
        return True

    # Converts expression to Number if possible.
    #   int to Integer
    #   float to approximate rational
    #   complex to Complex rational
    #   all other expressions, including Numbes are passed through
    def to_Number_if_possible(expression, max_denominator=100):
        if isinstance(expression, int):
            return Integer_from_int(expression)
        if isinstance(expression, float):
            return Rational_from_float(expression)
        if isinstance(expression, complex):
            return Complex_from_complex(expression)
        return expression

    def conjugate(self):
        return self.field.number_conjugate(self)

    def flip_sign(self):
        return self.field.number_flip_sign(self)

    def is_integer(self):
        return self.field.number_is_integer(self)

    def __float__(self):
        return self.field.number__float__(self)

    def __int__(self):
        return self.field.number__int__(self)

    def __str__(self):
        return self.field.number__str__(self)

    def __repr__(self):
        return self.field.number__repr__(self)

    def __complex__(self):
        return complex(self.field.number__complex__(self))
   
    def __add__(self, o):
        o = Number.to_Number_if_possible(o)
        if isinstance(o, Number):
            return self.field.operate('+', self, o)
        # It was not possible to operate within fields, create a Plus-expression
        return Expression.__add__(self, o)

    def __radd__(self, o):
        o = Number.to_Number_if_possible(o)
        if isinstance(o, Number):
            return self.field.operate('+', o, self)
        return Expression.__add__(o, self)

    def __sub__(self, o):
        o = Number.to_Number_if_possible(o)
        if isinstance(o, Number):
            return self.field.operate('-', self, o)
        return Expression.__sub__(self, o)

    def __rsub__(self, o):
        o = Number.to_Number_if_possible(o)
        if isinstance(o, Number):
            return o.field.operate('-', o, self)
        return Expression.__sub__(o, self)

    def __mul__(self, o):
        o = Number.to_Number_if_possible(o)
        if isinstance(o, Number):
            return self.field.operate('*', self, o)
        return Expression.__mul__(self, o)

    def __rmul__(self, o):
        o = Number.to_Number_if_possible(o)
        if isinstance(o, Number):
            return o.field.operate('*', o, self)
        return Expression.__add__(o, self)

    def __truediv__(self, o):
        o = Number.to_Number_if_possible(o)
        if isinstance(o, Number):
            return self.field.operate('/', self, o)
        return Expression.__truediv__(self, o)

    def __pow__(self, o):
        o = Number.to_Number_if_possible(o)
        if isinstance(o, Number):
            return self.field.operate('^', self, o)
        return Expression.__pow__(self, o)

    def __rtruediv__(self, o):
        o = Number.to_Number_if_possible(o)
        if isinstance(o, Number):
            return o.field.operate('/', o, self)
        return Expression.__truediv__(o, self)

    def __eq__(self, o):
        o = Number.to_Number_if_possible(o)
        if not isinstance(o, Number):
            return False
        return self.field.number__eq__(self, o)

    def __gt__(self, o):
        o = Number.to_Number_if_possible(o)
        if not isinstance(o, Number):
            return False
        return self.field.number__gt__(self, o)

    def simplify(self, verbose=0, lowlevel=False):
        return self.field.number_simplify(self)

    def simplify_isolate_factor(self, verbose=0):
        return self.field.number_simplify_isolate_factor(self)

    def is_number(self):
        return True

    def is_negative(self):
        return self.field.number_is_negative(self)

    def is_positive(self):
        return self.field.number_is_positive(self)

    def is_real(self):
        return self.field.number_is_real(self)

    def is_unity(self):
        return self.field.number_is_unity(self)

    def is_zero(self):
        return self.field.number_is_zero(self)

    def representation(self, caller=None, position=None):
        return self.field.number_representation(self)

    def has_leading_unary_operator(self):
        return self.field.number_has_leading_unary_operator(self)

IntegerField = IntegerFieldClass()
RationalField = RationalFieldClass()
ComplexField = ComplexFieldClass()

IntegerField.subset(RationalField).subset(ComplexField)

def Integer_from_int(expression):
    assert isinstance(expression, int)
    return Number(expression, IntegerField)

def Rational_from_float(expression, max_denominator=100):
    frac = builtin_Fraction( expression ).limit_denominator(max_denominator)
    if frac.denominator != 1:
        return Number( (frac.numerator, frac.denominator), RationalField)
    return Number(frac.numerator, IntegerField)

def Complex_from_complex(expression):
    return Number( (Rational_from_float(expression.real), Rational_from_float(expression.imag)), ComplexField)

def Fraction(A,B):
    assert(isinstance(A,int))
    assert(isinstance(B,int))
    return Number((A,B), RationalField)

def Complex(A,B):
    return Number((A,B), ComplexField)

Integer2 = Number(2, IntegerField)
Integer1 = Number(1, IntegerField)
Integer0 = Number(0, IntegerField)
IntegerMinus1 = Number(-1, IntegerField)
