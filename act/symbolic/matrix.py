from act.display.matrix import display_matrix
from act.symbolic.fields import Number, Integer0
from act.symbolic.expression import Operable, Expression
import numpy as np
# TODO: Separate common code from elementwise operations

class SymMatrix(Operable):
    operator_priority = 10

    def __init__(self, M):
        self.rows = len(M)
        self.cols = len(M[0])
        self.M = []
        self.listeners = []
        for i in range(self.rows):
            row = []
            for j in range(self.cols):
                row.append(Number.to_Number_if_possible(M[i][j]))
            self.M.append(row)

    def add_listener(self, listener):
        self.listeners.append(listener)

    def __setitem__(self, key, value):
        row, col = key
        self.M[row][col] = value
        for listener in self.listeners:
            listener.changed(self)

    def __getitem__(self, key):
        row, col = key
        return self.M[row][col]

    def __eq__(self, o):
        equal = True
        for i in range(self.rows):
            for j in range(self.cols):
                equal = equal and self.M[i][j] == o.M[i][j]
        return equal

    def __truediv__(self, o):
        if isinstance(o, Expression):
            M = [];
            for i in range(self.rows):
                row = []
                for j in range(self.cols):
                    row.append((self.M[i][j]/o))
                M.append(row)
            return SymMatrix(M)
        if isinstance(o, SymMatrix):
            M = []
            rows = len(o.M)
            cols = len(o.M[0])
            assert rows == self.rows
            assert cols == self.cols
            for i in range(self.rows):
                row = []
                for j in range(self.cols):
                    row.append(self.M[i][j] / o.M[i][j])
                M.append(row)
            return SymMatrix(M)

        raise NotImplementedError("/ operator not implemented for SymMatrix / %s" % o.__class__.__name__)

    def to_float(self):
        M = []
        for i in range(self.rows):
            row = []
            for j in range(self.cols):
                row.append(float(self.M[i][j]))
            M.append(row)
        return np.array(M)
        
    def __add__(self, o):
        if isinstance(o, Number):
            raise NotImplementedError
        if isinstance(o, SymMatrix):
            M = []
            rows = len(o.M)
            cols = len(o.M[0])
            assert rows == self.rows
            assert cols == self.cols
            for i in range(self.rows):
                row = []
                for j in range(self.cols):
                    row.append(self.M[i][j] + o.M[i][j])
                M.append(row)
            return SymMatrix(M)

        raise NotImplementedError

    def is_identity(self):
        for i in range(self.cols):
            for j in range(self.rows):
                if i != j:
                    if not self.M[i][j].is_zero():
                        return False
                if i == j:
                    if not self.M[i][j].is_unity():
                        return False
        return True

    def transpose(self):
        M = [];
        for i in range(self.cols):
            row = []
            for j in range(self.rows):
                row.append(self.M[j][i])
            M.append(row)
        return SymMatrix(M)

    def multiply(self, o):
        o_rows = o.rows
        o_cols = o.rows
        assert(self.cols == o.rows)
        M = []
        for i in range(self.rows):
            row = []
            for j in range(o.cols):
                value = Integer0
                for k in range(self.cols):
                    value = value + (self.M[i][k] * o.M[k][j])
                row.append(value)
            M.append(row)
        return SymMatrix(M)

    def __mul__(self, o):
        if isinstance(o, Number):
            M = [];
            for i in range(self.cols):
                row = []
                for j in range(self.rows):
                    row.append(o*self.M[j][i])
                M.append(row)
            return SymMatrix(M)
        raise NotImplementationError

    def conjugate(self):
        M = [];
        for i in range(self.rows):
            row = []
            for j in range(self.cols):
                row.append(self.M[i][j].conjugate())
            M.append(row)
        return SymMatrix(M)
    
    def __repr__(self):
        return repr(self.M)

    def simplify(self, verbose=0, lowlevel=False):
        M = [];
        for i in range(self.rows):
            row = []
            for j in range(self.cols):
                row.append(self.M[i][j].simplify(verbose, lowlevel))
            M.append(row)
        return SymMatrix(M)

    def representation(self, caller=None):
        return display_matrix(self.rows, self.cols, self.M, lambda x : x.representation())

    def inverse(self):
        from act.symbolic import sym
        assert(self.rows == self.cols)
        if self.rows == 2:
            a, b, c, d = self.M[0][0], self.M[0][1], self.M[1][0], self.M[1][1]
            idet = sym(1) / (a*d-b*c)
            return SymMatrix( [[ idet*d, -idet*b],
                               [-idet*c,  idet*a]])
        raise NotImplementedError

    def __str__(self):
        return self.representation()
