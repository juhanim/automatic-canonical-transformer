from act.symbolic.expression import Number, Minus, Plus
from act.symbolic.times import Times
from act.symbolic.integer import Integer

class Complex(Number):
    field_id = 1
    def __init__(self, r, i):
        self.r = r.simplify()
        self.i = i.simplify()

    def __str__(self):
        if self.r.is_zero():
            return "%sj" % str(self.i)
        if self.i.is_zero():
            return "%s" % str(self.r)
        return "(%s+%sj)" % (str(self.r), str(self.i))

    def __repr__(self):
        return "Complex(%s,%s)" % (str(self.r), str(self.i))

    def is_negative(self):
        return self.r.is_negative()

    def is_unity(self):
        return self.r.is_unity() and self.i.is_zero()

    def has_leading_unary_operator(self):
        return self.r.is_negative()

    def flip_sign(self):
        return Complex(self.r.flip_sign(), self.i.flip_sign())

    def representation(self, caller=None, position=None):
        if caller is None:
            return(str(self))
        if caller is not None:
            if position > 1:
                return "(%s)" % str(self)
        return str(self)

    def simplify(self):
        return Complex(self.r.simplify(), self.i.simplify())

    def simplify_isolate_factor(self):
        return None, self.simplify()

    def add_number(self, o):
        if o.field_id > self.field_id:
            return o.add_number(self)
        return Complex(self.r+o.r, self.i+o.i)

    def substract_number(self, o):
        if o.field_id > self.field_id:
            return o.substract_number_flipped(self)
        return Complex(self.r-o.r, self.i-o.i)

    def multiply_number(self, o):
        if o.field_id > self.field_id:
            return o.multiply_number(self)
        return Complex(Minus(Times(self.r, o.r), Times(self.i, o.i)), \
                       Plus(Times(self.r, o.i), Times(o.r, self.i)))

    def divide_number(self, o):
        if o.field_id > self.field_id:
            return o.divide_number_flipped(self)
        a, b = self.r, self.i
        if isinstance(o, Integer):
            return Complex(a/o, b/o)
        c, d = o.r, o.i
        return Div(Integer(1), c*c+d*d)*Complex(a*c+b*d, b*c-a*d)

    def is_number(self):
        return True

