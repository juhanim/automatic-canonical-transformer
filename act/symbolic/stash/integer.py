from act.symbolic.expression import Number

class Integer(Number):
    field_id = 1
    def __init__(self, value):
        self.value = int(value)

    def __str__(self):
        return "%d" % (self.value)

    def __repr__(self):
        return "Int(%d)" % self.value

    def is_negative(self):
        return self.value < 0

    def is_zero(self):
        return self.value == 0

    def is_unity(self):
        return self.value == 1

    def has_leading_unary_operator(self):
        return self.is_negative()

    def flip_sign(self):
        return Integer(-self.value)

    def representation(self, caller=None, position=None):
        if caller is None:
            return(str(self))
        if self.value >= 0:
            return(str(self))
        if caller is not None:
            if position > 1:
                return "(%s)" % str(self)
        return str(self)

    def simplify(self):
        return self

    def simplify_isolate_factor(self):
        return None, self

    def add_number(self, o):
        if o.field_id > self.field_id:
            return o.add_number(self)
        return Integer(self.value + o.value)

    def substract_number(self, o):
        if o.field_id > self.field_id:
            return o.add_number(self.flip_sign())
        return Integer(self.value - o.value)

    def multiply_number(self, o):
        if o.field_id > self.field_id:
            return o.multiply_number(self)
        return Integer(self.value * o.value)

    def divide_number(self, o):
        if isinstance(o, Integer):
            if self.value % o.value == 0:
                return Integer(self.value // o.value)
            from act.symbolic.fraction import Fraction
            return Fraction(self.value, o.value)
        if o.field_id > self.field_id:
            return o.divide_number_flipped(self)

    def is_number(self):
        return True

