from act.symbolic.expression import Number
from act.symbolic.integer import Integer

class Fraction(Number):
    field_id = 2
    def __init__(self, numerator, denominator):
        assert(isinstance(numerator,int))
        assert(isinstance(denominator,int))
        self.numerator = numerator
        self.denominator = denominator

    def __str__(self):
        return "%d/%d" % (self.numerator, self.denominator)

    def __repr__(self):
        return "Frac(%d, %d)" % (self.numerator, self.denominator)

    def __float__(self):
        return float(self.numerator) / self.denominator

    def is_negative(self):
        return self.numerator*self.denominator < 0

    def is_zero(self):
        return self.numerator == 0

    def is_unity(self):
        return self.numerator == self.denominator

    def has_leading_unary_operator(self):
        return self.is_negative()

    def flip_sign(self):
        return Fraction(-self.numerator, self.denominator)

    def representation(self, caller=None, position=None):
        if caller is None:
            return(str(self))
        if not self.is_negative:
            return(str(self))
        if caller is not None:
            if position > 1:
                return "(%s)" % str(self)
        return str(self)

    def simplify(self):
        from fractions import gcd
        factor = gcd(self.numerator, self.denominator)
        return Fraction(self.numerator // factor, self.denominator // factor)

    def simplify_isolate_factor(self):
        return None, self

    def add_number(self, o):
        if o.field_id > self.field_id:
            return o.add_number(self)
        if isinstance(o, Integer):
            return Fraction(self.numerator + o.value*self.denominator, self.denominator).simplify()
        if isinstance(o, Fraction):
            return Fraction(self.numerator*o.denominator + o.numerator*self.denominator, self.denominator*o.denominator).simplify()

    def substract_number(self, o):
        if o.field_id > self.field_id:
            return o.substract_number(self)
        if isinstance(o, Integer):
            return Fraction(self.numerator - o.value*self.denominator, self.denominator).simplify()
        if isinstance(o, Fraction):
            return Fraction(self.numerator*o.denominator - o.numerator*self.denominator, self.denominator*o.denominator).simplify()

    def multiply_number(self, o):
        if o.field_id > self.field_id:
            return o.multiply_number(self)
        if isinstance(o, Integer):
            return Fraction(self.numerator*o.value, self.denominator).simplify()
        if isinstance(o, Fraction):
            return Fraction(self.numerator*o.numerator, self.denominator*o.denominator).simplify()

    def is_number(self):
        return True

