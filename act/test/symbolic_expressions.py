from act.symbolic import sym, simplify
from act.display.symbols import Sym
a=sym(2)
b=sym("omega")
c=sym("Omega")
d=a*b
e=d/c
f=e.simplify()
for x in [a,b,c,d,e,f]:
    print(x.representation(), repr(x))

pairs = [ (sym("alpha"), Sym.alpha), 
          (sym("beta"), Sym.beta),
          (sym("gamma"), Sym.gamma),
          (sym("Omega"), Sym.Omega),
          (sym("omega"), Sym.omega),
          (sym("omega")+(sym(1)+sym(1j)), "((1+1i)+%s)" % Sym.omega),
          (sym("omega")+sym('a')+(sym(1)+sym(1j)), "((1+1i)+(a+%s))" % Sym.omega),
          (sym(3)*sym("Omega"), "3*"+Sym.Omega),
          (sym(3)*sym("Omega")*sym(2), "6*"+Sym.Omega), 
          (sym(3)*sym("omega")+sym(2)*sym("omega"), "5*"+Sym.omega),
          (sym(5)*sym("omega")/sym("Omega")-sym(2)*sym("omega")/sym("Omega"), "(3*%s)/%s" % (Sym.omega, Sym.Omega)) ]
soft_fail = False

for id, (symbol, representation) in enumerate(pairs):
    print(id)
    symbol = simplify(symbol)
    print(symbol.representation(), "Should be", representation)
    if symbol.representation() != representation:
        if soft_fail:
            print("^ERROR")
        else:
            raise Exception("Test failed:'%s' vs. '%s'. The inner structure: %s." % (symbol.representation(), representation, repr(symbol)))
