from act.symbolic import sym
from act.display.symbols import Sym
from act.test import random_fractions, ensure
from numpy.random import randint
import numpy as np

# Matrix multiplication
def test_matrix(a,b,c):
    float_A, sym_A = random_fractions((a,b))
    float_B, sym_B = random_fractions((b,c))
    float_C = np.dot(float_A, float_B)
    sym_C = sym.dot(sym_A, sym_B)
    ensure(float_C, sym_C.to_float())

for i in range(100):
    print(i)
    test_matrix(randint(1,10), randint(1,10), randint(1,10))

H1 = sym( [ [ 2, 3 ], [ 4, 5 ]])
H2 = sym( [ [ sym('alpha'), sym('beta') ],
           [ sym('omega'), sym('gamma') ]])
print(((H1+H2)/sym(2)).representation())
print((((H1+H2)/sym(2)).simplify()).representation())

H3 = sym( [ [ sym('a'), sym('b') ],
           [ sym('c')*sym(2)/sym(3), sym('d')+sym('b')*sym(3) ]])
H4 = sym( [ [ sym(3)*sym('a'), sym(4)*sym('b') ],
           [ sym(2)*sym('c')*sym(2)/sym(3), sym(4/5)*sym('d')+sym('b')*sym(3) ]])


steps = []
for H in [H1, H2, H3, H4]:
    Hinv = H.inverse()
    I = H.multiply(Hinv)
    print(H.representation())
    print("\n   TIMES    ")
    print(Hinv.representation())
    for i in range(15):
        print(I.representation())
        if I.is_identity():
            print("WORKS")
            break
        print("   ===")
        I = I.simplify()
    assert(i < 14)
    steps.append(i)
print("M*inv(M) tests simplified to identity in %s steps" % repr(steps))
assert( np.all( np.array(steps) <= np.array([1, 2, 2, 2]))) # Simplification cannot worsen

w = sym('omega')
wcav = sym('Omega')
g = sym('g');
I = sym(1j)
H4 = sym( [[ wcav + sym(2)*g**sym(2)/w,   sym(2)*g**sym(2)/w,  -g*I, g*I ],
          [        sym(2)*g**sym(2)/w,   wcav+sym(2)*g**sym(2)/w,  -g*I, g*I ],
          [ g*I,                         g*I,                     w,    sym(0) ], 
          [ -g*I,                       -g*I,                     sym(0),    w ]]).simplify()
print(H4.representation())    
