from numpy.random import randint
from act.symbolic import sym
import numpy as np

def random_fractions(size, high=12):
    numerator = randint(0, high, size=size)
    denominator = randint(1, high, size=size)
    return numerator/denominator, (sym(numerator)/sym(denominator)).simplify()
    
def ensure(A,B, eps=1e-10):
    D = np.abs(A-B)
    if not np.all(D<eps):
        raise Exception("Mismatch greater than %.5f.\n A=%s, B=%s." % (eps, repr(A), repr(B)))
