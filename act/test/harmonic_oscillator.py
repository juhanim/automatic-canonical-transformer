from act.symbolic import sym, simplify
from act.transformation import Transformation, DynamicalSystem
w = sym('omega')
H = sym([[ w, 0 ],
         [ 0, w ]])
J = sym([[  0, 1 ],
         [ -1, 0 ]])
D = sym([[  0, 1 ],
         [  1, 0 ]])

system = DynamicalSystem(H,J,D)

#system.display()

U = Transformation(sym([ [1,  1],
                         [1j, -1j]])/sym(2)**sym(1/2))
system = U(system)
system.display()

assert( system.H == sym( [[ w, 0], [0, w]]))
assert( system.J == sym( [[ 0, 1j], [-1j, 0]]))
assert( system.D.is_identity() )
assert( system.EOM == sym( [[0,-1j*w],[1j*w,0]]))
# H =
#   [ 𝜔                             0                              ]
#   [ 0                             𝜔                              ]
# 
# J =
#   [ 0                             1i                             ]
#   [ -1i                           0                              ]
# 
# D =
#   [ 1                             0                              ]
#   [ 0                             1                              ]
# 
# e.o.m. =
#   [ 0                             -1i*𝜔                          ]
#   [ 1i*𝜔                          0                              ]

print("Adding 2K q^2 term.")
system.H[0,0] = system.H[0,0] + sym('K')*sym(2)
system.display()

# Dynamical system
# H =
#  [ (𝜔+K)                         0                              ]
#  [ 0                             𝜔                              ]
#
# J =
#  [ 0                             1i                             ]
#  [ -1i                           0                              ]
#
# D =
#  [ 1                             0                              ]
#  [ 0                             1                              ]
#
# i d . /dt = [ H, . ] =
#  [ 0                             -1i*𝜔                          ]
#  [ 1i*((K+𝜔))                    0                              ]
#

# Transform back
system = U.inverse()(system)
system.display()

