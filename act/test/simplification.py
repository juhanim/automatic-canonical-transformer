from act.symbolic import sym, simplify
from act.symbolic.binary_operators import Plus, Minus, Div, Times

tests = [ (Plus(0,1), "1"),
          (Plus(1,0), "1"),
          (Plus(sym('a'), sym('a')), "2*a"),
          (Plus(sym('a'), 2*sym('a')), "3*a"),
          (Times(sym('a'), sym('a')), "a^(2)"),
          (Times(Times(sym('a'), sym('a')), sym('a')), "a^(3)"),
          (Plus(Plus(sym('a'),sym('b')), Plus(sym('b'),2*sym('a'))), "(3*a+2*b)"),
          (Times(2, sym('a')), "2*a"),
          (Times(sym('a'), 2), "2*a"),
          (Times(Times(sym('a'),sym('b')), Times(sym('b'),2*sym('a'))), "2*a^(2)*b^(2)") ]
for symbols, ref_text in tests:
    print("Before:", repr(symbols))
    symbols = simplify(symbols)
    print("After:", repr(symbols))
    text = symbols.representation()
    print(text, ref_text)
    assert text == ref_text
