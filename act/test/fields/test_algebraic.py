from act.symbolic import sym
from act.symbolic.fields import Number, RationalField, IntegerField, Integer1
import numpy as np

def randnum():
    while True:
        num = randint(-5,5)
        if not num == 0:
            return num

def randfrac():
    a, b = randnum(), randnum()
    return sym(a) / b, a/b
from random import randint

def random_operator(depth=3):
    if randint(0,100)<20:
        a, aval = randfrac()
        arep = "(%s)" % str(a)
        return a, aval, arep
    if depth == 0:
        a, aval = randfrac()
        b, bval = randfrac()
        arep, brep = str(a), str(b)
        arep = "(%s)" % arep
        brep = "(%s)" % brep
    else:
        a, aval, arep = random_operator(depth-1)
        b, bval, brep = random_operator(depth-1)
    if bval == 0 or aval == 0:
        op = randint(1,3)
    else:
        op = randint(1,5)

    if op == 5 and abs(bval) > 12:
        op = 1

    if op == 5 and abs(aval) >0 and abs(aval)<1e-5:
        op = 1

    try:
        if op == 1:
            rep = "(%s+%s)" % (arep, brep)
            expr_value = a+b
            value = aval + bval
        if op == 2:
            rep = "(%s-%s)" % (arep, brep)
            expr_value = a-b
            value = aval - bval
        if op == 3:
            rep = "(%s*%s)" % (arep, brep)
            expr_value = a*b
            value = aval * bval
        if op == 4:
            rep = "(%s/%s)" % (arep, brep)
            expr_value = a/b
            value = aval / bval
        if op == 5:
            rep = "(%s^%s)" % (arep, brep)
            expr_value = a**b
            value = aval ** bval
    except (OverflowError, ZeroDivisionError):
        # Operation failed, just return one of the operands directly
        return a, aval, arep        
    evaluated_expr = complex(expr_value)
    diff = abs(evaluated_expr - value)
    if np.isnan(value):
        return Integer1, 1, "(1)"
    if (abs(value)<1 and diff>1e-8) or (abs(value)>1 and diff/abs(value) > 1e-8):
        print("a = ", repr(a))
        print("b = ", repr(b))
        print(str(a), op, str(b), "total", rep, "num", value, "algebra", complex(expr_value))
        asd
    return expr_value, value, rep

lens = 0
for N in range(10000):
    x, value, xrep = random_operator()
    print("Iteration:", N)
    #print(xrep)
    lens += len(str(x))
    #print(repr(x.simplify()))
    #print("Floating point value:", complex(x))
    #print("Numeric             :", value)
    diff = abs(complex(x)-value)
    print("Difference ", diff)
    if abs(value)<1:
        assert(diff<1e-10)
    else:
        assert(diff / abs(value) < 1e-10)
print("Average repr len", lens/10000)

# average length without optimizations 26
