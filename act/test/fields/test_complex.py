from act.symbolic import sym
from act.symbolic.fields import Fraction, Complex
from random import randrange
def test_fraction():
    def compare(expression, ref_value):
        value = complex(expression)
        print(value, ref_value)
        assert(abs(value-ref_value)<1e-10)
        
    A = Fraction(randrange(1,20), randrange(1,20))
    B = Fraction(randrange(1,20), randrange(1,20))
    C = Fraction(randrange(1,20), randrange(1,20))
    D = Fraction(randrange(1,20), randrange(1,20))
    U = Complex(A,B)
    V = Complex(C,D)
    W = U / V
    compare(W, (float(A)+1j*float(B))/(float(C)+1j*float(D)))
    W = U + V
    compare(W, (float(A)+1j*float(B))+(float(C)+1j*float(D)))
    W = U * V
    compare(W, (float(A)+1j*float(B))*(float(C)+1j*float(D)))
    W = U - V
    compare(W, (float(A)+1j*float(B))-(float(C)+1j*float(D)))

#a = sym(2j)
#b = sym(3)
#c = a/b
#d = c.simplify()
#for x in [a,b,c,d]:
#    print(x.representation())

pairs = [ (sym(2)/sym(3), "2/3", 2/3),
          (sym(3+1j)+sym(5+3j), "(8+4i)", 8+4j),
          (sym(4) + sym(3+1j)+sym(5+3j), "(12+4i)", 12+4j),
          (4 + sym(1)/sym(2) + sym(3+1j)+sym(5+3j), "(25/2+4i)", 25/2+4j),
          (sym(1j)-sym(1j), "0", 0),
          (sym(5j)-sym(3j), "2i", 2j),
          (sym(5j)-sym(7j), "-2i", -2j),
          (sym(1j)*sym(1j), "-1", -1),
          (sym(2j)/sym(3), "2/3i", 2j/3),
          (sym(1)+sym(1j), "(1+1i)", 1+1j),
          ((sym(2)/sym(3)+sym(3j)/sym(5))*(sym(9)/sym(6)+sym(11j)/sym(5)), "(-8/25+71/30i)", -8/25+71j/30) ]
soft_fail = False
id=0
for symbol, representation, value in pairs:
    id=id+1
    print("XXX",id)
    symbol = symbol.simplify()
    print(symbol.representation(), representation, value, symbol.simplify().representation())
    print(repr(symbol.simplify()))
    if symbol.representation() != representation:
        if soft_fail:
            print("^ERROR")
        else:
            raise Exception("Test failed:'%s' vs. '%s'." % (symbol.representation(), representation))
    if abs(complex(value) - complex(symbol))>1e-10:
        if soft_fail:
            print("^ERROR")
        else:
            raise Exception("Test failed.", complex(value), complex(symbol))




for i in range(100):
    test_fraction()   

