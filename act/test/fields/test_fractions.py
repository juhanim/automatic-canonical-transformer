from act.symbolic import sym
from act.symbolic.fields import Number
assert isinstance(sym(1), Number)
assert str(sym(1).field) == "IntegerField"

pairs = [ (sym(1)/sym(2), "1/2", 0.5),
          (sym(12)/sym(1), "12", 12),
          (sym(1)/sym(3)+sym(4)/sym(6), "1", 1),
          (sym(1)/sym(2)-sym(1)/sym(3), "1/6", 1/6),
          (sym(4)/sym(3)*sym(3)/sym(4), "1", 1),
          ((sym(4)/sym(3))*(sym(3)/sym(4)), "1", 1),
          ((sym(-4)/sym(3))*(sym(-3)/sym(4)), "1", 1),
          ((sym(-4)/sym(3))*(sym(3)/sym(4)), "-1", -1),
          (sym(3)/sym(7)-2, "-11/7", -11/7),
          (sym(1)+sym(1)/(sym(1)+sym(1)/(sym(1)+sym(1)/(sym(1)+sym(1)/(sym(1)+sym(1))))),"13/8", 13/8),
          ((sym(5)/sym(-9)) / (sym(3)/sym(6)),"-10/9", -10/9), 
          ( sym(233)/sym(100)+sym(99), "10133/100", 10133/100) ]
soft_fail = False
for expression, reference_representation, reference_numerical_value in pairs:
    expression = expression.simplify()
    representation = expression.representation()
    numerical_value = float(expression)
    print(representation,' vs. ',reference_representation,' | ', numerical_value, 'vs. ', reference_numerical_value)
    if representation != reference_representation:
        if soft_fail:
            print("^ERROR")
        else:
            raise Exception("Test failed '%s' vs. '%s'." % (representation, reference_representation))
    if abs(numerical_value-reference_numerical_value)>1e-10:
        if soft_fail:
            print("^ERROR")
        else:
            raise Exception("Test failed.")




