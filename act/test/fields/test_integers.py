from act.symbolic import sym
from act.symbolic.fields import Number
assert isinstance(sym(1), Number)
assert str(sym(1).field) == "IntegerField"

pairs = [ (sym(1), "1", 1),
          (sym(12), "12", 12),
          (sym(-0), "0", 0),
          (sym(-24), "-24", -24),
          (sym(3)+sym(7), "10", 10),
          (sym(-5)+sym(9),"4", 4),
          (sym(5)+sym(-9),"-4", -4),
          (sym(3)*sym(4), "12", 12),
          (sym(-3)*sym(4), "-12", -12),
          (sym(3)*sym(-4), "-12", -12),
          (sym(-3)*sym(-4), "12", 12),
          (sym(2)+sym(3)*sym(4), "14", 14),
          (sym(2)*sym(3)+sym(4), "10", 10),
          ((sym(2)+sym(3))*sym(4), "20", 20),
          (sym(-2)+sym(3)*sym(4), "10", 10),
          (sym(-2)*sym(3)+sym(4), "-2", -2),
          ((sym(-2)+sym(3))*sym(4), "4", 4),
          (sym(-2)+sym(-3)*sym(4), "-14", -14),
          (sym(-2)*sym(-3)+sym(4), "10", 10),
          ((sym(-2)+sym(-3))*sym(4), "-20", -20),
          (sym(-2)+sym(-3)*sym(-4), "10", 10),
          (sym(-2)*sym(-3)+sym(-4), "2", 2),
          ((sym(-2)+sym(-3))*sym(-4), "20", 20),
          (((sym(2)+sym(1)*sym(3))*sym(7)-sym(2)+sym(2)*sym(2)*(sym(-2)-sym(2))), "17", 17)]
soft_fail = False
for symbol, representation, value in pairs:
    print(symbol.representation(), representation, value, symbol.simplify().representation())
    if symbol.representation() != representation:
        if soft_fail:
            print("^ERROR")
        else:
            raise Exception("Test failed:'%s' vs. '%s'." % (symbol.representation(), representation))
    if str(value) != symbol.simplify().representation():
        if soft_fail:
            print("^ERROR")
        else:
            raise Exception("Test failed:'%s' vs. '%s'." % (str(value), symbol.simplify().representation()))




