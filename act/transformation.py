from act.symbolic import sym, simplify

class DynamicalSystem:
    def __init__(self, H, J, D, title="Dynamical system"):
        self._H = H
        self._J = J
        self._D = D
        self._EOM = None
        self.title = title
        for x in [H,J,D]:
            x.add_listener(self)
        self.update_equation_of_motion()

    def __setattr__(self, key, value):
        super().__setattr__(key, value)
        if not key.startswith('_'):
            self.update_equation_of_motion()

    def __getattr__(self, key):
        if key == 'H':
            return self._H
        if key == 'J':
            return self._J
        if key == 'D':
            return self._D
        if key == 'EOM':
            return self._EOM
        raise AttributeError(key)

    def changed(self, x):
        self.update_equation_of_motion()

    def display(self):
        print("Dynamical system")
        print(" H = ")
        print(self.H.representation())
        print(" J = ")
        print(self.J.representation())
        print(" D = ")
        print(self.D.representation())
        print(" i d . /dt = [ H, . ] = ")
        print(self.EOM.representation())

    def HJD(self):
        return (self.H, self.J, self.D)

    def update_equation_of_motion(self):
        H, J, D = self.HJD()
        DTH = simplify(D.transpose().multiply(H))
        self._EOM = simplify(J.transpose().multiply(DTH+DTH.transpose())/sym(2))

class Transformation:
    def __init__(self, M):
        self.M = M
        self.invM = M.inverse()
        self.invM = simplify(self.invM)
        self.invMT = self.invM.transpose()
        self.conjM = M.conjugate()

    def inverse(self):
        return Transformation(simplify(self.M.inverse()))

    # Perform the transformation to the system
    def __call__(self, system):
        print("Transforming with")
        print(" M =")
        print(self.M.representation())
        (H, J, D), M, invMT, invM, conjM = system.HJD(), self.M, self.invMT, self.invM, self.conjM
        # Dp = np.dot(M.conjugate(), np.dot(D, invM))
        Dp = simplify(conjM.multiply(D.multiply(invM).simplify()))
        # Hp = np.dot(Dp, np.dot(invMT, np.dot(D.tranpose(), np.dot(H, invM))))
        Hp = Dp.multiply(simplify(invMT.multiply(simplify(D.transpose().multiply(simplify(H.multiply(invM)))))))
        print(Hp.representation())
        Hp = simplify(Hp)

        # Jp = np.dot(invM, np.dot(J, invMT))
        Jp = simplify(invM.multiply(J.multiply(invMT)))

        return DynamicalSystem(Hp, Jp, Dp, title=system.title)
