class Sym:
    Omega = "\N{Mathematical Italic Capital Omega}"
    omega = "\N{Mathematical Italic Small Omega}"
    gamma = "\N{Mathematical Italic Small Gamma}"
    alpha = "\N{Mathematical Italic Small Alpha}"
    beta  = "\N{Mathematical Italic Small Beta}"

def pretty(x):
    for repr, character in Sym.__dict__.items():
        if repr.startswith('_'):
            continue
        x = x.replace(repr, character)
    return x
