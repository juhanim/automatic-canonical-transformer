def display_matrix(rows, cols, M, repr_fun, pre_text="", column_width=30):
    text = ""
    pre_rows = pre_text.split('\n')
    w = max(map(len, pre_rows))
    for i in range(rows):
        row = ("%%-%ds  [ " % w) % (pre_rows[i] if i < len(pre_rows) else "")
        for j in range(cols):
            row += ("%%-%ds" % column_width) % repr_fun(M[i][j])
        row += " ]"
        text += row + "\n"
    return text

